<?php

$today = date('Y-m-d', time());

require_once '../classes/Course.php';
require_once('../classes/DB.php');
require_once('../functions/generalFunctions.php');
$db=new DB();
$db->setConnection();
$date=$_POST['day'];
$link=$date;


if(!empty($_POST['location'])){
	$link.='&location='.$_POST['location'];
}

if (!empty($_POST['location']) && !empty($_POST['day'])){
	$shop=$db->getShop($_POST['location']);
	$courses=$db->getFreeCoursesByShop($date, $shop);
}

else {

//echo $_POST['trainer'];
//echo $_POST['location'];

$dateNoFormat=$_POST['day'];
$courses=$db->getFreeCourses($date);
$text=$_POST['day'];
}
?>
<ul class="events-list">
<?php
if($date < $today){
    echo "Please select a date in future";
}
else if (empty($courses)){
	echo "No courses found for this Date";
}
else {


    foreach ($courses as $course) {
        ?>
        <li class="col-sm-6 col-md-6 col-lg-4">
            <div class="events-list-inner">
                <div class="first-block">
                    <div class="pull-right">
                        <span class="style-normal"><?php echo $course->getHour(); ?></span>
                        <div class="time-icons">
                            <span class="style-super">:00</span> <span class="style-sub">AM</span>
                        </div>
                    </div>
                </div>
                <div class="second-block">
                    <a href="?site=BookCourse&course=<?php echo $course->getId(); ?>&date=<?php echo $course->getDate(); ?>&hour=<?php echo $course->getHour(); ?>"><?php echo $course->getName(); ?>
                        <span class="style-super"></span></a>
                    <hr>
                    <span class="event-by"><?php echo $course->getShop()->getCity(); ?>
                        , <?php echo $course->getShop()->getStreet(); ?> <?php echo $course->getSlots(); ?></span>
                </div>
            </div>
        </li>


        <?php
    }
}
?>
</ul>

