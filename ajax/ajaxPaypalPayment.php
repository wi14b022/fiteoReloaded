<?php

require_once('../classes/DB.php');
require_once('../classes/Customer.php');
require_once('../classes/FitpointPackage.php');
require_once('../functions/generalFunctions.php');
session_start();
$db=new DB();
$db->setConnection();
set_time_limit (1200);
$time_start = microtime(true);
$error="";
if ($_POST)
{
	if (!isset($_POST['package'])){
		$error.="CreditCard Type is missing<br />";
	}
	else {
		$fitpointPackage=$db->getFitpointsPackage($_POST['package']);
	}
	
	if (!isset($_POST['firstname'])){
		$error.= "Firstname is missing";
	}
	if (!isset($_POST['lastname'])){
		$error.= "Lastname is missing";
	}
	if (!isset($_POST['creditcardtype'])){
		$error.= "CreditCard type is missing";
	}
	if (!isset($_POST['ccNumber'])){
		$error.= "CreditCard number is missing";
	}
	if (!isset($_POST['month'])){
		$error.= "CreditCard month is missing";
	}
	if (!isset($_POST['year'])){
		$error.= "CreditCard year is missing";
	}
	if (!isset($_POST['cvv'])){
		$error.= "CreditCard cvv is missing";
	}
}


require_once('../pp-sdk/vendor/autoload.php');
require_once('../pp-sdk/lib/PayPal/Rest/ApiContext.php');
require_once('../pp-sdk/lib/PayPal/Api/Address.php');
require_once('../pp-sdk/lib/PayPal/Api/CreditCard.php');
require_once('../pp-sdk/lib/PayPal/Api/FundingInstrument.php');
require_once('../pp-sdk/lib/PayPal/Api/Payer.php');
require_once('../pp-sdk/lib/PayPal/Api/Amount.php');
require_once('../pp-sdk/lib/PayPal/Api/Transaction.php');
require_once('../pp-sdk/lib/PayPal/Api/Payment.php');

use PayPal\Rest\ApiContext;
use PayPal\Api\Address;
use PayPal\Api\CreditCard;
use PayPal\Api\Amount;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Auth\OAuthTokenCredential;


$apiContext = new ApiContext(new OAuthTokenCredential(
		"Acuz_NNQp6lq9BVV9Eye_HoBBKqxkBMqJ_2qSfUMxpw3CAHiOBB6grMGjLtJ0yPsSuapE46i4_r55fWT", "EFjcvx-Jn5NkUOmQmvk17Okt-NwAPXxe9zkc8V0fl4GJRkZL1kzlbGYe0vIlpRWMuJfo7yL55DmzrepT"));

if (empty($error)){
	
	
	$addr = new Address();
	$addr->setLine1('Rennweg 97');
	$addr->setCity('Vienna');
	$addr->setCountryCode('AT');
	$addr->setPostalCode('1030');
	
	$card = new CreditCard();
	$card->setNumber($_POST['ccNumber']);
	$card->setType($_POST['creditcardtype']);
	$card->setExpireMonth($_POST['month']);
	$card->setExpireYear($_POST['year']);
	$card->setCvv2($_POST['cvv']);
	$card->setFirstName($_POST['firstname']);
	$card->setLastName($_POST['lastname']);
	$card->setBillingAddress($addr);
	
	$fi = new FundingInstrument();
	$fi->setCreditCard($card);
	
	$payer = new Payer();
	$payer->setPaymentMethod('credit_card');
	$payer->setFundingInstruments(array($fi));
	
	
	$amount = new Amount();
	$amount->setCurrency('EUR');
	$amount->setTotal($fitpointPackage->getPrice());
	//$amount->setDetails($amountDetails);
	
	$transaction = new Transaction();
	$transaction->setAmount($amount);
	$transaction->setDescription('This is the payment transaction description.');
	
	$payment = new Payment();
	$payment->setIntent('sale');
	$payment->setPayer($payer);
	$payment->setTransactions(array($transaction));
	
	try {
	    $payment->create($apiContext);
	} catch (PayPal\Exception\PayPalConnectionException $ex) {
	    //echo $ex->getCode(); // Prints the Error Code
	    //echo $ex->getData(); // Prints the detailed error message 
	    $json = $ex->getData();
	    
	    $obj = json_decode($json);
	    //print $obj->{'foo-bar'}; // 12345
	    //var_dump($obj->{'details'}[0]);
	    
	    if (isset($obj->{'details'})){
	    	foreach ($obj->{'details'} as $detail){
	    		$det=preg_split("/\./", $detail->{'field'});
	    		if (isset($det[2])) $error.=ucfirst($det[2]).' ';
	    		if (isset($det[3])) $error.=ucfirst($det[3]).' - ';
	    		if (isset($detail->{'issue'})) $error.=$detail->{'issue'};
	    		echo $error.'<br>';
	    		$error='';	
	    }
	    
	    }
	    
	    $file = 'people.txt';
	    file_put_contents($file, $ex->getData().PHP_EOL, FILE_APPEND);
	    //die($ex);
	} catch (Exception $ex) {
	    //die($ex);
	}
	
	$time_end = microtime(true);
	$execution_time = ($time_end - $time_start)/60;
	echo $payment->getState();
	
	if (trim($payment->getState())=="approved"){
		$db->insertPackage($_SESSION['currentUser'], $fitpointPackage);
		$_SESSION['currentUser']->setFitpoints($_SESSION['currentUser']->getFitpoints()+$fitpointPackage->getFitpoints());
	}
	
	
	$file = 'paypalMonitoring.txt';
	file_put_contents($file, $payment->getState().'	', FILE_APPEND);
	file_put_contents($file, $execution_time.PHP_EOL, FILE_APPEND);
}
else echo $error;
?>
