<?php 
require_once '../classes/Trainer.php';
require_once '../classes/FreeHour.php';
require_once('../classes/DB.php');
require_once('../functions/generalFunctions.php');
$db=new DB();
$db->setConnection();
$date=$_POST['day'];
$link=$date;

if(!empty($_POST['location'])){
	$link.='&location='.$_POST['location'];
}

if ((!empty($_POST['trainer']) || $_POST['trainer']=="0") && !empty($_POST['day'])){
	$trainer = new Trainer();
	$trainer->setId($_POST['trainer']);
	$db->getTrainer($trainer);
	if ($_POST['trainer']=="0"){
		$hours=$db->getHours();
		$text=$_POST['day'];
		$dateNoFormat=$_POST['day'];
	}
	else {
		$hours=$db->getFreeTrainerDate($trainer, $_POST['day']);
		$text=$_POST['day'].' with '.$trainer->getFirstName().' '.$trainer->getLastName();
	}
	
	$link.='&trainer='.$trainer->getId();
}

else {

//echo $_POST['trainer'];
//echo $_POST['location'];

$dateNoFormat=$_POST['day'];
$hours=$db->getHours();

}
?>
<ul class="events-list">
<?php 

if (empty($hours)){
	echo "No Trainers found for this Date";
}


foreach($hours as $hour)
{
?>
	<li class="col-sm-6 col-md-6 col-lg-4">
		<div class="events-list-inner">
			<div class="first-block">
				<div class="pull-right">
					<span class="style-normal"><?php echo $hour->getHour(); ?></span>
					<div class="time-icons">
						<span class="style-super">:00</span> <span class="style-sub">AM</span>
					</div>
				</div>
			</div>
			<div class="second-block">
				<a href="?site=requestMeeting&date=<?php echo $link; ?>&hour=<?php echo $hour->getHour(); ?>"><?php echo $hour->getDescription();?><span class="style-super"></span></a>
				<hr>
				<span class="event-by"><?php echo $text; ?></span>
			</div>
		</div>
	</li>
	

<?php 
}
?>
</ul>

