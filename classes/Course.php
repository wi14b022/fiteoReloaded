<?php
/********************************
Project:	Case Study 4 - FH Technikum Wien
Author:		Daniel Perschy
Date:		2016-04-11

Description: 
Basic Class for FITEO, contains Course data
********************************/


require_once 'Shop.php';
class Course
{
    private $date;
    private $hour;
    private $id;
    private $name;
    private $count;
    private $countBookings;
    private $shop;
    
    public function getId(){
    	return $this->id;
    }
    
    public function setId($value){
    	$this->id=$value;
    }
    
    public function getName(){
    	return $this->name;
    }
    
    public function setName($value){
    	$this->name=$value;
    }

    public function getDate(){
    	return $this->date;
    }
    
    public function setDate($value){
    	$this->date=$value;
    }
    
    public function getHour(){
    	return $this->hour;
    }
    
    public function setHour($value){
    	$this->hour=$value;
    }
    
    public function getCount(){
    	return $this->count;
    }
    
    public function setCount($value){
    	$this->count=$value;
    }
    
    public function getBookings(){
    	return $this->countBookings;
    }
    
    public function setBookings($value){
    	$this->countBookings=$value;
    }
    
    public function getShop(){
    	return $this->shop;
    }
    
    public function setShop($value){
    	$this->shop=$value;
    }
    
 	public function getSlots(){
    	return "(".$this->getBookings()."/".$this->getCount().")";
    }
    
    

   

}
?>