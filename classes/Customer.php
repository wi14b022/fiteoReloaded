<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Basic Class for FITEO, contains Customer 
 specific data derives from User general details
 ********************************/


require_once 'ETR.php';
class Customer extends User
{
    private $lifestyle;
    private $size;
    private $diet;
    private $status;
    private $fitpoints;
    private $trainer;
    private $unlockedProducts=array();
    private $userETRs=array();
    private $trainerETRs=array();

    
    public function setUnlockedProducts($value){
    	$this->unlockedProducts=$value;
    	//array_push($this->unlockedProducts, $value);
    }
    
    public function getUnlockedProducts(){
    	return $this->unlockedProducts;
    }
    
    public function setStatus($value){
    	$this->status=$value;
    }
    
    public function getStatus(){
    	return $this->status;
    }
    
    public function setFitpoints($value){
    	$this->fitpoints=$value;
    }
    
    public function getFitpoints(){
    	return $this->fitpoints;
    }
    
    public function setSize($value){
    	$this->size=$value;
    }
    
    public function getSize(){
    	return $this->size;
    }
    
    public function setDiet($value){
    	$this->diet=$value;
    }
    
    public function getDiet(){
    	return $this->diet;
    }
    
    public function setLifestyle($value){
    	$this->lifestyle=$value;
    }
    
    public function getLifestyle(){
    	return $this->lifestyle;
    }
    
    public function setTrainer($value){
    	$this->trainer=$value;
    }
    
    public function getTrainer(){
    	return $this->trainer;
    }
    
    
    public function getStatusReadable(){
    	if ($this->status=="U"){
    		return LockingState::UNBLOCKED;
    	}
    	else if ($this->status=="B"){
    		return LockingState::BLOCKED;
    	}
    	else return LockingState::UNKNOWN;
    }
    
    public function getStatusReadableInverse(){
    	if ($this->status=="B"){
    		return LockingState::UNBLOCKED;
    	}
    	else if ($this->status=="U"){
    		return LockingState::BLOCKED;
    	}
    	else return LockingState::UNKNOWN;
    }
    
    public function toogleStatus(){

    	if ($this->status=="U"){
    		$this->status="B";
    	}
    	else $this->status="U";
    	
    	
    }

    
    
    
    public function addETR($name, $duration, $date){
    	$newETR=new ETR($name, $duration, $date);
    	if($newETR->valid){
    		array_push($this->etr, $newETR);
    	}
    }
    
    public function loadETR($id, $name, $duration, $date, $lock){	
    	array_push($this->etr, new ETR($id, $name, $duration, $date, $lock));
    }
    
    public function setUserETRs($value){
    	$this->userETRs=$value;
    }
    
    
    public function getUserETRs()
    {
    	return $this->userETRs;
    }
    
    public function setTrainerETRs($value){
    	$this->trainerETRs=$value;
    }
    
    
    public function getTrainerETRs()
    {
    	return $this->trainerETRs;
    }

}
?>