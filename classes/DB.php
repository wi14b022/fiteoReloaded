<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Basic Class for FITEO, uses Fiteo Classes
 the only Class for Database operations, can be easily replaced
 when other database technology should be used
 ********************************/


require_once 'User.php';
require_once 'Customer.php';
require_once 'Trainer.php';
require_once 'Video.php';
require_once 'PredefinedPlan.php';
require_once 'Shop.php';
require_once 'FreeHour.php';
require_once 'Exercise.php';
require_once 'Image.php';
require_once 'Course.php';

class DB
{
	private $servername = "localhost";
	private $username = "fiteoReloaded";
	private $password = "fiteoReloaded";
	private $db = "fiteoReloaded";
	private $conn;
	
	
	// Create connection
	
	
	public function setConnection()
	{
		$this->conn = new mysqli($this->servername, $this->username, $this->password, $this->db);
	}
	
	/*
	 * 
	 * DB Transactions for User / Customer / Employee / Trainer Tables;
	 * 
	 */
	
	public function getUserType($login){
		$sql="SELECT * FROM customer WHERE customerID IN 
				(SELECT userID FROM user WHERE Login='".$login."')";
		logging($sql);
		$results=$this->conn->query($sql);
		if($results->num_rows==1){
			return "Customer";
		}
		 
		 
		$sql="SELECT * FROM employee WHERE userID IN 
				(SELECT userID FROM user WHERE Login='".$login."')";
		logging($sql);
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			return "Employee";
		}
		 
		$sql="SELECT * FROM trainer WHERE userID IN 
				(SELECT userID FROM user WHERE Login='".$login."')";
		logging($sql);
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			return "Trainer";
		}
	
		if($results->num_rows==0){
			unset($user);
			addErrorMessage("Username or Password not valid");
		}
	
	}
	
	public function getUser(User $user){
		$sql="SELECT * FROM user WHERE Login='".$user->getLogin()."' and Passwort='".$user->getPassword()."' AND KZ_Loesch='0'";
		//array_push(, $value1)
	
		 
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			$user->setId($row->UserID);
			$this->getUserById($user);
		}
			
		if($results->num_rows==0){
			unset($user);
			addErrorMessage("Username or Password not valid");
			return false;
		}
		else if ($user instanceof Customer) {
			$this->getCustomer($user);
		}
		return true;
	}
	
	public function getUserById(User $user){
		$sql="SELECT * FROM user WHERE userid='".$user->getId()."'";
		//array_push(, $value1)
	
	
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			$user->setCity($row->Ort);
			$user->setCountry($row->Land);
			$user->setEmail($row->Email);
			$user->setFirstName($row->Vorname);
			$user->setId($row->UserID);
			$user->setLogin($row->Login);
			$user->setPassword($row->Login);
			$user->setLastName($row->Nachname);
			$user->setPhoneNumber($row->Telefonnr);
			$user->setPlz($row->PLZ);
			$user->setSex($row->Geschlecht);
			$user->setStreet($row->Strasse);
		}
	
		if ($user instanceof Customer){
			$this->getCustomer($user);
		}
	
		if($results->num_rows==0){
			unset($user);
			addErrorMessage("Username or Password not valid");
		}
	}
	
	public function getCustomers(){
		$sql="SELECT * FROM customer, user WHERE customerid=userid and user.KZ_Loesch='0'";
		$customers=array();
	
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			$customer = new Customer();
			$customer->setId($row->UserID);
			$this->getUserById($customer);
			array_push($customers, $customer);
		}
		return $customers;
	}
	

	
	public function getCustomer(Customer $cust){
		//$this->getUser($cust);
		 
		$sql="SELECT * FROM customer WHERE customerId=".$cust->getId()."";
		//array_push(, $value1)
		 
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			$cust->setStatus($row->Status);
			$cust->setFitpoints($row->Fitpoints);
			$cust->setSize($row->Groesse);
			$cust->setDiet($row->Ernaehrung);
			$cust->setLifestyle($row->Lifestyle);
			$cust->setTrainer($row->Stammtrainer);
	
	
		}
		 
		$this->getUnlockedProducts($cust);
		$this->getUserEtrArray($cust);
		$this->getTrainerEtrArray($cust);
	
		if($results->num_rows==0){
			unset($cust);
			addErrorMessage("Its not a Customer");
		}
		 
		 
	}
	
	
	
	public function getCustomerFitpoints(Customer $cust){
		$sql="SELECT * FROM customer WHERE customerId=".$cust->getId()."";
		$results=$this->conn->query($sql);
		while($row = $results->fetch_object()) {
			$cust->setFitpoints($row->Fitpoints);
	
		}
	}

	public function checkUserNameIsNotUsed($username){
		$sql="SELECT * FROM user WHERE Login='".$username."'";
		$results=$this->conn->query($sql);
		if($results->num_rows==0){
			return true;
		}
        return false;
	}

    public function checkEmailIsNotUsed($email){
        $sql="SELECT * FROM user WHERE Email='".$email."'";
        $results=$this->conn->query($sql);
        if($results->num_rows==0){
            return true;
        }
        return false;

    }
	
    public function saveCustomer(Customer $user){
		$sql="INSERT INTO user (Vorname, Nachname, Email, Telefonnr, 
				Login, Passwort, PLZ, Strasse, Ort, Land, Geschlecht, Aenddatum) 
				VALUES ('".$user->getFirstName()."', '".$user->getLastName()."', 
						'".$user->getEmail()."', '".$user->getPhoneNumber()."', '".$user->getLogin()."', 
						'".$user->getPassword()."', '".$user->getPlz()."', '".$user->getStreet()."', 
						'".$user->getCity()."', '".$user->getCountry()."', '".$user->getSex()."', CURRENT_TIME());";
    	
		$last_inserted_id=$this->insert($sql, "Customer angelegt", "Fehlgeschlagen");
		
		if (!empty($last_inserted_id)){
			$user->setId($last_inserted_id);
			$sql="INSERT INTO customer (CustomerID, Status, Aenddatum) 
					VALUES (".$user->getId().", 'B', CURRENT_TIME())";
			$this->insert($sql,  "", "Fehlgeschlagen");
		}

    }
    
    public function updateUser(User $user){
    	    
    	$sql="UPDATE user SET Vorname = '".$user->getFirstName()."', 
    			Nachname = '".$user->getLastName()."', 
    			Email = '".$user->getEmail()."',
    	 		Telefonnr = '".$user->getPhoneNumber()."', 
    	 		Login = '".$user->getLogin()."', 
    	 		Passwort = '".$user->getPassword()."', 
    	 		PLZ = '".$user->getPlz()."', 
    	 		Strasse = '".$user->getStreet()."',
    	 		Ort = '".$user->getCity()."', 
    	 		Land = '".$user->getCountry()."', 
    	 		Geschlecht = '".$user->getSex()."',
    	 		Aenddatum = CURRENT_TIME() 
    	 		WHERE user.UserID = ".$user->getId().";";
    	 logging($sql);
    	$last_inserted_id=$this->insert($sql, "", "User Update fehlgeschlagen");
    	
    	if ($user instanceof Customer){
    		$this->updateCustomer($user);
    	}
    }

    public function deleteUser(User $user){
        $sql="UPDATE user SET KZ_Loesch='1',
              Aenddatum = CURRENT_TIME() 
    	 		WHERE user.UserID = ".$user->getId().";";
        logging($sql);
        $last_inserted_id=$this->insert($sql, "", "User delete fehlgeschlagen");
        if ($user instanceof Customer){
            $this->deleteCustomer($user);
        }
    }

    public function deleteCustomer(User $user){
        $sql="UPDATE customer SET KZ_Loesch='1',
              Aenddatum = CURRENT_TIME() 
    	 		WHERE CustomerID = ".$user->getId().";";
        logging($sql);
        $last_inserted_id=$this->insert($sql, "", "Customer delete fehlgeschlagen");
    }
    
    public function updateCustomer(Customer $user){

    	if(empty($user->getTrainer())){
    		$trainer="NULL";
    	}
    	else $trainer=$user->getTrainer();
    	
    	$sql="UPDATE customer SET Status='".$user->getStatus()."',
    			lifestyle='".$user->getLifestyle()."',
    			groesse='".$user->getSize()."',
    			ernaehrung='".$user->getDiet()."',
    			stammtrainer=".$trainer.",
    			Aenddatum = CURRENT_TIME()  			
    			WHERE customerId = ".$user->getId().";";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "Customer was updated", "An error occured");
    }
    
    
    /*
     *
     * DB Transactions for Trainer Tables;
     *
     */
    

    
    public function getTrainer(Trainer $trainer){
    	$sql="SELECT * FROM user u, trainer t WHERE 
    			u.userid=t.userid and t.userid=".$trainer->getId()."";
    
    	logging("DB.php getTrainer() SQL: ".$sql);
    
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$trainer->setId($row->UserID);
    		$trainer->setCity($row->Ort);
    		$trainer->setCountry($row->Land);
    		$trainer->setEmail($row->Email);
    		$trainer->setFirstName($row->Vorname);
    		$trainer->setLastName($row->Nachname);
    		$trainer->setPhoneNumber($row->Telefonnr);
    		$trainer->setLogin($row->Login);
    		$trainer->setSex($row->Geschlecht);
    		$trainer->setStreet($row->Strasse);
    		//array_push($trainers, $trainer);
    	}
    }
    
    
    
    public function getTrainerArray(){
    	$sql="SELECT * FROM user u, trainer t WHERE u.userid=t.userid AND t.userId=0
    			UNION
    			SELECT * FROM user u, trainer t WHERE u.userid=t.userid";
    	$trainers=array();
    	 
    	logging("DB.php getTrainerArray() SQL: ".$sql);
    
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$trainer = new Trainer();
    		$trainer->setId($row->UserID);
    		$trainer->setCity($row->Ort);
    		$trainer->setCountry($row->Land);
    		$trainer->setEmail($row->Email);
    		$trainer->setFirstName($row->Vorname);
    		$trainer->setLastName($row->Nachname);
    		$trainer->setPhoneNumber($row->Telefonnr);
    		$trainer->setLogin($row->Login);
    		$trainer->setSex($row->Geschlecht);
    		$trainer->setStreet($row->Strasse);
    		array_push($trainers, $trainer);
    	}
    	return $trainers;
    }
    

    
    /*
     *
     * DB Transactions for ETR Tables;
     *
     */
    
    
    public function getUserEtrArray(Customer $customer){

        $sql="SELECT * FROM etr WHERE CustomerUserID=".$customer->getId()." and Trainingsart in ('A','K')";
    	logging("DB.php	getUserEtrArray() sql: ".$sql);
    	$etrList=array();

    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		logging("DB.php	getUserEtrArray() RecordID: ".$row->RecordID);
    		$etr=new ETR();
    		$etr->setId($row->RecordID);
    		$etr->setRecorder($row->Aufzeichner);
    		$etr->setDuration($row->Dauer);
    		$etr->setRecordType($row->Trainingsart);
    		$etr->setTrainer($row->TrainerUserID);
    		array_push($etrList, $etr);
    	}
    	$customer->setUserETRs($etrList);
    	return $etrList;
    }

    public function getTrainerEtrArray(Customer $customer){

    	$sql="SELECT * FROM etr WHERE CustomerUserID=".$customer->getId()." and aufzeichner='T' and Trainingsart='M'";
    	logging("DB.php	getTrainerEtrArray() sql: ".$sql);
    	$etrList=array();

    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		logging("DB.php	getTrainerEtrArray() RecordID: ".$row->RecordID);
    		$etr=new ETR();
    		$etr->setId($row->RecordID);
    		$etr->setRecorder($row->Aufzeichner);
    		$etr->setDuration($row->Dauer);
    		$etr->setAdipose($row->Koerperfett);
    		$etr->setPuls($row->Pulsschlag);
    		$etr->setMuscleMass($row->Muskelmasse);
    		$etr->setRecordType($row->Trainingsart);
    		$etr->setTrainer($row->TrainerUserID);
    		array_push($etrList, $etr);
    	}
    	$customer->setTrainerETRs($etrList);
    	return $etrList;
    }
    
    public function updateETR(ETR $etr){
    
    	$sql="UPDATE etr SET 
    			Aufzeichner='".$etr->getRecorder()."',
    			Pulsschlag=".$etr->getPuls().",
    			Dauer=".$etr->getDuration().",
    			Koerperfett=".$etr->getAdipose().",
    			Muskelmasse=".$etr->getMuscleMass().",
    			Trainingsart='".$etr->getRecordType()."',
    			TrainerUserID=".$etr->getTrainer().",
    			Aenddatum = CURRENT_TIME() 
    			WHERE RecordID = ".$etr->getId().";";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "ETR updated", "Some error occured");
    }
    
    public function insertETR(ETR $etr, Customer $customer){
    
    	$sql="INSERT INTO etr (Aufzeichner, Pulsschlag, Dauer, Koerperfett, 
    			Muskelmasse, Trainingsart, TrainerUserID, CustomerUserID, AendDatum) VALUES (
    			'".$etr->getRecorder()."',
    			".$etr->getPuls().",
    			".$etr->getDuration().",
    			".$etr->getAdipose().",
    			".$etr->getMuscleMass().",
    			'".$etr->getRecordType()."',
    			".$etr->getTrainer().",
    			".$customer->getId().",
    			CURRENT_TIME());";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "ETR inserted", "Some error occured");
    	$etr->setId($last_inserted_id);
    }
    
    

    /*
     *
     * DB Transactions for UnlockedProducts, Plans, PredefinedPlan, Exercises, MedienContents 
     *
     */
    
    public function getUnlockedProducts(Customer $customer){
    	$sql="SELECT * FROM unlockedproduct WHERE userid=".$customer->getId()."";
    	//array_push(, $value1)
    	
    	$unlockedProducts=array();
    	logging("DB.php	getUnlockedProducts() sql: ".$sql);
    	 
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$product=new Product();
    		logging("DB.php	getUnlockedProducts() row: ".$row->PlanID);
    		if(!empty($row->PlanID)){
    			$product=new Plan();
    			$product->setId($row->PlanID);
    			$newPlan=$this->getPlanById($product);
    			array_push($unlockedProducts, $newPlan);
    		}
    		
    		if(!empty($row->MedienID)){
    			$product=new MedienContent();
    			$product->setId($row->MedienID);
    			$newMedienContent=$this->getMedienContentById($product);
    			array_push($unlockedProducts, $newMedienContent);
    		}
    	}
    	
    	$customer->setUnlockedProducts($unlockedProducts);
    }
    
    
    public function getMedienContentById(MedienContent $medienContent){
    	$sql="SELECT * FROM mediencontent WHERE MedienID=".$medienContent->getId()."";
    	logging("DB.php	getMedienContentById() sql: ".$sql);

    	 
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		
    		if ($row->ContentTyp=='V'){
    			$medienContent=new Video();
    			$medienContent->setFitpoints($row->FitPointWert);
    		}
    		if ($row->ContentTyp=='P'){
    			$medienContent=new Image();
    		}
    		
    		$medienContent->setId($row->MedienID);
    		$medienContent->setName($row->Bezeichnung);
    		$medienContent->setUrl($row->URL);
    		logging("DB.php	getMedienContentById() URL: ".$row->URL);
    		$medienContent->setThumbnail($row->Thumbnail);
    		
    	}
    	return $medienContent;
    	
    }
    
    public function getPlanById(Plan $plan){
    	$sql="SELECT * FROM plan WHERE planid=".$plan->getId()."";
    	logging("DB.php	getPlanById() sql: ".$sql);
    	//array_push(, $value1)
    	$newPlan=new Plan(); 
    	
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$plan->setId($row->PlanID);
    		$plan->setName($row->Bezeichnung);
    		$plan->setDescription($row->Beschreibung);
    		$plan->setThumbnail($row->Thumbnail);
    		$plan->setSportType($row->Sportart);
    		$plan->setPlanType($row->PlanTyp);
    		
    		$plan->setExercises($this->getExercisesToPlan($plan));
    		
    		logging("DB.php	getPlanById() Bezeichnung: ".$row->Bezeichnung);
    	}
    	return $plan;
    
    }
    
    public function getExercisesToPlan(Plan $plan){
    	$sql="SELECT * FROM exercise e, exercisetoplan etp WHERE 
    			e.ExerciseID=etp.ExerciseID and e.KZ_Loesch=0 and etp.KZ_Loesch=0 and etp.PlanID=".$plan->getId()."
    			ORDER BY Reihenfolge ASC";
    	logging("DB.php	getExercisesToPlan() sql: ".$sql);
    	//array_push(, $value1)
    	$exercises=array();
    	 
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$exercise = new Exercise();
    		logging("DB.php	getExercisesToPlan() Bezeichnung: ".$row->Bezeichnung);
    		$exercise->setName($row->Bezeichnung);
    		$exercise->setDescription($row->Beschreibung);
    		$exercise->setOrder($row->Reihenfolge);
    		
    		if (!empty($row->MedienID)){
    			$mediencontent = new MedienContent();
    			$mediencontent->setId($row->MedienID);
    			//$mediencontent=$thi
    			
    			$exercise->setMedienContent($this->getMedienContentById($mediencontent));
    		}
    		
    		
    		$exercise->setRevision($row->Wiederholungen);
    		$exercise->setQuantity($row->Saetze);
    		$exercise->setDuration($row->Dauer);
    		$exercise->setStartWeight($row->Startgewicht);
    		array_push($exercises, $exercise);
    	}
    	return $exercises;
    
    }
    
    public function getProduct(Product $product){
    	if ($product instanceof Video){
    		$sql="select medienId as id, bezeichnung as bezeichnung,
    				fitpointwert as fitpointwert from mediencontent where medienid=".$product->getId()."";
    	}
    
    	if ($product instanceof PredefinedPlan){
    		$sql="select planid as id, bezeichnung as bezeichnung,
    				fitpointwert as fitpointwert from predefinedplan where planid=".$product->getId()."";
    	}
    	logging($sql);
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		if($product instanceof PredefinedPlan){
    			//$product->setDescription($row->beschreibung);
    			//$product->setSportType($row->sportart);
    			//$product->setPlanType($row->plantyp);
    		}
    		else if ($product instanceof Video){
    			 
    		}
    		$product->setName($row->bezeichnung);
    		$product->setId($row->id);
    		$product->setFitpoints($row->fitpointwert);
    	}
    }
    
    public function getBuyableProductsArray(Customer $cust){
    	$products=array();
    	$sql="select p.planId AS id, pdp.bezeichnung AS bezeichnung, fitpointwert as wert, p.beschreibung as beschreibung,
				p.plantyp as plantyp, p.sportart as sportart, pdp.thumbnail as thumbnail, 'PREDEFINEDPLAN' AS TYP
				from Plan p, PREDEFINEDPLAN pdp WHERE p.planId=pdp.planid and p.planid not IN (SELECT planID from unlockedproduct WHERE UserId=".$cust->getId()." and planid is not null)
				union
				select medienId as id, bezeichnung as bezeichnung, fitpointwert as wert, '' as beschreibung,
				'' as plantyp, '' as sportart, thumbnail as thumbnail, 'VIDEO' AS TYP
				from MEDIENCONTENT WHERE ContentTyp='V' and FitPointWert > 0 AND MedienID not IN (SELECT MedienID from unlockedproduct WHERE UserId=".$cust->getId()." and medienId is not null)";
    	logging("DB.php	getBuyableProductsArray() sql: ".$sql);
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    
    		if($row->TYP=="PREDEFINEDPLAN"){
    			$product=new PredefinedPlan();
    			$product->setDescription($row->beschreibung);
    			$product->setSportType($row->sportart);
    			$product->setPlanType($row->plantyp);
    		}
    		else if ($row->TYP="VIDEO"){
    			$product=new Video();
    		}
    		$product->setName($row->bezeichnung);
    		$product->setId($row->id);
    		$product->setFitpoints($row->wert);
    		$product->setThumbnail($row->thumbnail);
    
    		array_push($products, $product);
    
    	}
    	return $products;
    }
    
    
    
    public function checkUserHasEnoughFitpoints(Product $product, Customer $customer){
    	$this->getProduct($product);
    	 
    	$sql="SELECT * from customer WHERE fitpoints-".$product->getFitpoints().">=0
    			and customerId=".$customer->getId()."";
    	logging($sql);
    	$results=$this->conn->query($sql);
    	if ($results->num_rows) return true;
    	addErrorMessage("Not enough Fitpoints");
    	return false;
    	 
    }
    

    public function checkCustomerHasProduct(Product $product, Customer $customer){
    	if ($product instanceof Video) {
    		$sql="SELECT * FROM unlockedproduct WHERE medienId=".$product->getId()." and UserID=".$customer->getId()."";
    	}
    	 
    	if ($product instanceof PredefinedPlan){
    		$sql="SELECT * FROM unlockedproduct WHERE planid=".$product->getId()." and UserID=".$customer->getId()."";
    	}
    	 
    	$results=$this->conn->query($sql);
    	logging("checkCustomerHasProduct SQL".$sql);
    	if($results->num_rows==0){
    
    		return false;
    	}
    	 
    	logging("checkCustomerHasProduct has not the product ");
    	addErrorMessage("This Product did you already bought");
    	return true;
    }
    
    public function buyProduct(Product $product, Customer $customer){
    	 
    	$this->getProduct($product);
    	 
    	if ($this->checkUserHasEnoughFitpoints($product, $customer) && !$this->checkCustomerHasProduct($product, $customer))
    	{    		 
    		if ($product instanceof Video) {    	   
    			$sql="INSERT INTO unlockedproduct (Aenddatum, FitpointWert, medienId, UserID)
	    			VALUES (CURRENT_TIME(), ".$product->getFitpoints().",
	    			".$product->getId().", ".$customer->getId().")";
    		}
    
    		if ($product instanceof PredefinedPlan){
    			$sql="INSERT INTO unlockedproduct (Aenddatum, FitpointWert, planid, UserID)
	    			VALUES (CURRENT_TIME(), ".$product->getFitpoints().",
	    			".$product->getId().", ".$customer->getId().")";
    		}
    
    		logging($sql);
    		$last_inserted_id=$this->insert($sql, "Product succesfully buyed", "Error occured");
    		logging("Costs: ".$product->getFitpoints());
    		$sql="UPDATE customer SET Fitpoints = Fitpoints - ".$product->getFitpoints()." WHERE customerID=".$customer->getId()."";
    		logging($sql);
    		$last_inserted_id=$this->insert($sql, "", "");
    		$customer->setFitpoints($customer->getFitpoints()-$product->getFitpoints());
    	}
    	 
    }
    
 
    
    /*
     *
     * DB Transactions for Courses
     *
     */
    
   
    
    public function getFreeCourses($date){
    	$sql=" SELECT * FROM (
    		SELECT s.DATUM as Datum, s.BebuchteStunde as Stunde, s.CourseID as CourseID, 
    				c.Bezeichnung as Bezeichnung, c.MaximalePlaetze as Plaetze, 
    					count(b.Datum) As AnzahlBuchungen, FilialID as ShopID
    		FROM schedule s
    		LEFT JOIN course c ON s.CourseID = c.CourseID
    		LEFT JOIN bookedschedule b ON c.CourseID = b.CourseID AND s.Datum = b.Datum
    		WHERE s.Datum='".$date."'
    		GROUP BY s.Datum, s.BebuchteStunde, s.CourseID
		    ) AS T
		    WHERE Plaetze > AnzahlBuchungen";
    	$courses=array();
    
    	logging("DB.php getFreeCourses() SQL: ".$sql);
    	 
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$course = new Course();
    		$course->setId($row->CourseID);
    		$course->setName($row->Bezeichnung);
    		$course->setDate($row->Datum);
    		$course->setHour($row->Stunde);
    		$course->setCount($row->Plaetze);
    		$course->setBookings($row->AnzahlBuchungen);
    		$course->setShop($this->getShop($row->ShopID));
    		
    		array_push($courses, $course);
    	}
    	return $courses;
    }
    
    public function getFreeCoursesByShop($date, Shop $shop){
    	$sql=" SELECT * FROM (
    		SELECT s.DATUM as Datum, s.BebuchteStunde as Stunde, s.CourseID as CourseID,
    				c.Bezeichnung as Bezeichnung, c.MaximalePlaetze as Plaetze,
    					count(b.Datum) As AnzahlBuchungen, FilialID as ShopID
    		FROM schedule s
    		LEFT JOIN course c ON s.CourseID = c.CourseID
    		LEFT JOIN bookedschedule b ON c.CourseID = b.CourseID AND s.Datum = b.Datum
    		WHERE s.Datum='".$date."'
    		AND FilialID = ".$shop->getID()."
    		GROUP BY s.Datum, s.BebuchteStunde, s.CourseID
		    ) AS T
		    WHERE Plaetze > AnzahlBuchungen";
    	$courses=array();
    
    	logging("DB.php getFreeCourses() SQL: ".$sql);
    
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$course = new Course();
    		$course->setId($row->CourseID);
    		$course->setName($row->Bezeichnung);
    		$course->setDate($row->Datum);
    		$course->setHour($row->Stunde);
    		$course->setCount($row->Plaetze);
    		$course->setBookings($row->AnzahlBuchungen);
    		$course->setShop($this->getShop($row->ShopID));
    
    		array_push($courses, $course);
    	}
    	return $courses;
    }
    
    public function setCourse(Customer $customer, Course $course){

    	$sql="INSERT INTO bookedschedule (Aenddatum, Buchungsdatum, userid, bebuchteStunde, CourseID, Datum)
    			VALUES (CURRENT_TIME(), CURRENT_TIME(),
    			".$customer->getId().", ".$course->getHour().",
    			".$course->getId().", '".$course->getDate()."')";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "Course was succesfully booked", "Course already booked, please choose another one");
    }
    
    /*
     *
     * DB Transactions for TrainerCalendar
     *
     */
    
    public function getFreeTrainerDate(Trainer $trainer, $date){
    	$sql="SELECT * FROM stunde WHERE stunde not in (SELECT BebuchteStunde from trainercalendar 
    			where UserID=".$trainer->getId()." and datum='".$date."')";
    	$hours=array();
    	 
    	logging("DB.php getFreeTrainerDate() SQL: ".$sql);
    	
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$hour = new FreeHour();
    		$hour->setHour($row->stunde);
    		$hour->setDescription($row->text);
    		array_push($hours, $hour);
    	}
    	return $hours;	
    }
    

    /*
     *
     * DB Transactions for Requests, TrainerCalendar
     *
     */
    

    
    public function setRequest(Request $request){
    	$this->conn->begin_transaction();
    	$sql="INSERT INTO request (Aenddatum, TrainerUserID, CustomerUserId, FilialID, Status)
    			VALUES (CURRENT_TIME(),
    			".$request->getTrainer()->getId().", ".$request->getUser()->getId().", 
    			".$request->getFiliale().",  'O')";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "", "");
    	$request->setId($last_inserted_id);
    	
    	if ($request->getDate()!="NULL" && $request->getHour()!="NULL"){
    		if($this->setTrainingCalendar($request)) {
    			
    		}
    		else {
    			
	    		$this->conn->rollback();
	    		return false;
    		}
    	}
    	
    	$this->conn->commit();
    }
    
    public function setChangePlanRequest(Request $request){
    
    	$sql="INSERT INTO request (Aenddatum, TrainerUserID, CustomerUserId, PlanID, notes , Status)
    			VALUES (CURRENT_TIME(),
    			".$request->getTrainer()->getId().", ".$request->getUser()->getId().",
    			".$request->getPlan()->getId().",".$request->getNotes().", 'O')";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "Request was succesfully saved", "");

    }
    
    public function setTrainingCalendar(Request $request){
    	$id=$request->getTrainer()->getId();
    
    	if($this->checkUserHasNoMeetingRequested($request)){
    		$sql="INSERT INTO trainercalendar (Aenddatum, userid, bebuchteStunde, requestId, datum)
    			VALUES (CURRENT_TIME(),
    			".$id.", ".$request->getHour().",
    			".$request->getId().", '".$request->getDate()."')";
    		logging($sql);
    		$last_inserted_id=$this->insert($sql, "Request was successfully sent", "");
    		return true;
    	}
    	else {
    		return false;
    	}
    	
    }
    
    public function checkUserHasNoMeetingRequested(Request $request){
    	$sql="SELECT * FROM trainercalendar WHERE RequestID IN
    	(SELECT RequestID from request WHERE CustomerUserID=".$request->getUser()->getId().")
    	AND DATUM='".$request->getDate()."' AND BebuchteStunde='".$request->getHour()."'";
    	 
    	$results=$this->conn->query($sql);
    	 
    	if ($results->num_rows==0) return true;
    	addErrorMessage("You have already requested for this Date and user");
    	return false;
    	 
    }
    
    
    /*
     *
     * DB Transactions for Filiale
     *
     */
    
    public function getShop($shopID){
    	$sql="SELECT * FROM filiale where FilialID = '".$shopID."'";
   
    	logging("DB.php getShopArray() SQL: ".$sql);
    
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$shop = new Shop();
    		$shop->setId($row->FilialID);
    		$shop->setCity($row->Ort);
    		$shop->setCountry($row->Land);
    		$shop->setEmail($row->Email);
    		$shop->setPhoneNumber($row->Telefonnummer);
    		$shop->setStreet($row->Strasse);

    	}
    	return $shop;
    }
    
    
    public function getShopArray(){
    	$sql="SELECT * FROM filiale where KZ_Loesch=0";
    	$shops=array();
    	 
    	logging("DB.php getShopArray() SQL: ".$sql);
    
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$shop = new Shop();
    		$shop->setId($row->FilialID);
    		$shop->setCity($row->Ort);
    		$shop->setCountry($row->Land);
    		$shop->setEmail($row->Email);
    		$shop->setPhoneNumber($row->Telefonnummer);
    		$shop->setStreet($row->Strasse);
    		array_push($shops, $shop);
    	}
    	return $shops;
    }
    
    /*
     *
     * DB Transactions for Fitpoints packages
     *
     */
    
    
    public function getFitpointsPackages(){
    	$sql="SELECT * FROM fitpointpackage WHERE KZ_Loesch=0 order by verkaufswert";
    	$fitpoints=array();
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$fitpointsPackage = new FitpointPackage();
    		$fitpointsPackage->setId($row->PackageID);
    		$fitpointsPackage->setFitpoints($row->FitpointsWert);
    		$fitpointsPackage->setPrice($row->Verkaufswert);
    		logging("DB.php	getFitpointsPackages() loaded id: ".$fitpointsPackage->getId());
    		array_push($fitpoints, $fitpointsPackage);
    	}    	
    	return $fitpoints;
    }
    
    public function getFitpointsPackage($id){
    	logging("DB.php	getFitpointsPackage() id:".$id);
    	$sql="SELECT * FROM fitpointpackage WHERE KZ_Loesch=0 and packageId=".$id."";
    	logging("DB.php	getFitpointsPackage() sql: ".$sql);
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$fitpointsPackage = new FitpointPackage();
    		$fitpointsPackage->setId($row->PackageID);
    		$fitpointsPackage->setFitpoints($row->FitpointsWert);
    		$fitpointsPackage->setPrice($row->Verkaufswert);
    		logging("DB.php	getFitpointsPackage() loaded id: ".$fitpointsPackage->getId());
    	}
    	return $fitpointsPackage;
    }
    
    public function insertPackage(User $user, FitpointPackage $fitpoint){
    		
    	$sql="INSERT INTO orderedfitpoint (OrderNummer, Aenddatum, OrderDatum, 
    			BezahlterWert, BezahlteFitpoints, UserID, PackageID, KZ_Loesch) 
    			VALUES (NULL, CURRENT_TIME(), 
    			CURRENT_TIME(), ".$fitpoint->getPrice().", ".$fitpoint->getFitpoints().", ".$user->getId().", ".$fitpoint->getId().", '0')";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "", "");
    	
    	$sql="UPDATE customer SET Fitpoints = Fitpoints + ".$fitpoint->getFitpoints()." WHERE customerID=".$user->getId()."";
    	logging($sql);
    	$last_inserted_id=$this->insert($sql, "", "");
    	 
    }
    



    
    
    /*
     *
     * DB Transactions Help Functions
     *
     */
    

    private function query($sql, $succMsg, $errMsg){
    
    	if ($this->conn->query($sql) === TRUE) {
    		if (!empty($succMsg)) addSuccessMessage($succMsg);
    		return $result;
    	} else {
    		//addErrorMessage(constant('ErrorMessage::'.'MYSQL_'.$this->conn->errno));
    		logging("Error: - " . $sql . "<br>" . $this->conn->error);
    	}
    }
    
    
    
    
    
    public function insert($sql, $succMsg, $errMsg){
    	 
    	if ($this->conn->query($sql) === TRUE) {
    		if (!empty($succMsg)) addSuccessMessage($succMsg);
    		return $this->conn->insert_id;
    	} else {
    		addErrorMessage($errMsg);
    		logging("Error: - " . $sql . "<br>" . $this->conn->error);
    	}
    }
    
    private function returnNull($var){
    	 if (empty($var)){
    	 	return "NULL";
    	 }
    	 return $var;
    }
    
    
    public function getHours(){
    	$sql="SELECT * FROM stunde";
    	$hours=array();
    
    	logging("DB.php getHours() SQL: ".$sql);
    
    	$results=$this->conn->query($sql);
    	while($row = $results->fetch_object()) {
    		$hour = new FreeHour();
    		$hour->setHour($row->stunde);
    		$hour->setDescription($row->text);
    		array_push($hours, $hour);
    	}
    	return $hours;
    }
    
    
    
}
?>