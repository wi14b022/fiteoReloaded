<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Basic Class for FITEO, contains Employee 
 specific data derives from User general details
 ********************************/

require_once 'User.php';
class Employee extends User
{
    private $lifestyle;
    private $groesse;
    private $ernaehrung;
    private $status;
    private $fitpoints;
    private $stammtrainer;
    public $etr=array();
    

    
    
    
    public function addETR($name, $duration, $date){
    	$newETR=new ETR($name, $duration, $date);
    	if($newETR->valid){
    		array_push($this->etr, $newETR);
    	}
    }
    
    public function loadETR($id, $name, $duration, $date, $lock){	
    	array_push($this->etr, new ETR($id, $name, $duration, $date, $lock));
    }
    
    public function getETR($id)
    {
    	foreach ($this->etr as $etr)
    	{
    		if ($etr->id == $id){
    			return $etr;
    		}
    	}
    }

}
?>