<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Exercise, used in Plan,  each Plan can consists a 
 list of Exercises. Some Exercises has MedienContents, 
 Images or Videos
 ********************************/


require_once 'MedienContent.php';

class Exercise
{
    private $name;
    private $description;
    private $medienContent;
    private $order;
    
    private $quantity;
    private $revision;
    private $duration;
    private $startweight;
    
    public function getOrder(){
    	return $this->order;
    }
    
    public function setOrder($value){
    	$this->order=$value;
    }

    public function getName(){
    	return $this->name;
    }
    
    public function setName($value){
    	$this->name=$value;
    }
    
    public function getDescription(){
    	return $this->description;
    }
    
    public function setDescription($value){
    	$this->description=$value;
    }
    
    public function getMedienContent(){
    	return $this->medienContent;
    }
    
    public function setMedienContent($value){
    	$this->medienContent=$value;
    }
    
    public function getRevision(){
    	if ($this->revision!=0){
    		return $this->revision.' times';
    	}
    	return "";
    }
    
    public function setRevision($value){
    	$this->revision=$value;
    }
    
    public function getQuantity(){
    	if ($this->quantity!=0){
    		return $this->quantity.' times';
    	}
    	return "";
    }
    
    public function setQuantity($value){
    	$this->quantity=$value;
    }
    
    public function getDuration(){
    	if ($this->duration!=0){
    		return $this->duration.' min';
    	}
    	return "";
    }
    
    public function setDuration($value){
    	$this->duration=$value;
    }
    
    public function getStartWeight(){
    	if ($this->startweight!=0){
    		return $this->startweight.' kg';
    	}
    	return "";
    }
    
    public function setStartWeight($value){
    	$this->startweight=$value;
    }
    

   

}
?>