<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Fitpoint Packages is buyable for the 
 Customer, these class holds the data
 ********************************/


class FitpointPackage {
	private  $id;
	private $fitpoints;
	private $price;
	
	public function setId($value){
		$this->id=$value;
	}
	
	public function getId(){
		return $this->id;
	}
	
	public function setFitpoints($value){
		$this->fitpoints=$value;
	}
	
	public function getFitpoints(){
		return $this->fitpoints;
	}
	
	public function setPrice($value){
		$this->price=$value;
	}
	
	public function getPrice(){
		return $this->price;
	}
	
	public function getName(){
		return $this->fitpoints.' Fitpoints - &euro; '.$this->price.'.-';
	}
}
?>