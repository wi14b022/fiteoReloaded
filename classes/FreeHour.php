<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Class contains the free hour and the basic description
 is used in request Meeting process.
 ********************************/


class FreeHour
{
    private $hour;
    private $description;
    
    public function setHour($value){
    	$this->hour=$value;
    }
    
    public function getHour(){
    	return $this->hour;
    }
    
    public function setDescription($value){
    	$this->description=$value;
    }
    
    public function getDescription(){
    	return $this->description;
    }
}
?>