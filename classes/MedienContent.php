<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:  
 Basic Class for FITEO,
 MedienContent can be a image or an Video
 ********************************/


require_once 'Product.php';

class MedienContent extends Product
{
	private $url;

    public function getUrl(){
    	return $this->url;
    }
    
    public function setUrl($value){
    	$this->url=$value;
    }
    
}
?>