<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description: 
 Plan consists exercises, sporttype, plantype and a general description.
 Its not buyable directly, only predefined plans can be buyed.
 ********************************/


class Plan extends Product
{
    private $sporttype;
    private $plantype;
    private $description;
    private $exercises;
    
    public function getSportType(){
    	return $this->sporttype;
    }
    
    public function setSportType($value){
    	$this->sporttype=$value;
    }
    
    public function getPlanType(){
    	if ($this->plantype=="T"){
    		return "Trainingplan";
    	}
    	else if ($this->plantype=="D"){
    		return "Dietplan";
    	}
    	else $this->plantype;
    	
    }
    
    public function setPlanType($value){
    	$this->plantype=$value;
    }
    
    public function getDescription(){
    	return $this->description;
    }
    
    public function setDescription($value){
    	$this->description=$value;
    }
    
    public function getExercises(){
    	return $this->exercises;
    }
    
    public function setExercises($value){
    	$this->exercises=$value;
    }
    
    public function getBuyLink(){
    	return "type=plan&id=".parent::getId();
    }
    
    public function getType(){
    	return "Plan";
    }
  

}
?>