<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:
 PredefinedPlan are directly buyable
 ********************************/


require_once 'Plan.php';

class PredefinedPlan extends Plan
{
    private $fitpoints;
    
    public function getFitpoints(){
    	return $this->fitpoints;
    }
    
    public function setFitpoints($value){
    	$this->fitpoints=$value;
    }
    
    public function getBuyLink(){
    	return "type=plan&id=".parent::getId();
    }
  

}
?>