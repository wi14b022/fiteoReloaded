<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:
 Basic class for buyable MedienContents and Plans,
 holds the general data.
 ********************************/


class Product
{
	private $id="NULL";
    private $name;
    private $thumbnail;

    public function getId(){
    	return $this->id;
    }
    
    public function setId($value){
    	$this->id=$value;
    }
    
    public function getName(){
    	return $this->name;
    }
    
    public function setName($value){
    	$this->name=$value;
    }
    
    public function getThumbnail(){
    	return $this->thumbnail;
    }
    
    public function setThumbnail($value){
    	$this->thumbnail=$value;
    }
    

   

}
?>