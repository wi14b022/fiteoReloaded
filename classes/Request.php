<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:
 Request can be either for an Meeting or changing an
 existing plan.
 ********************************/


class Request
{
	private $id="NULL";
    private $notes="NULL";
    private $trainer="NULL";
    private $user="NULL";
    private $plan="NULL";
    private $filiale="NULL";
    private $date="NULL";
    private $hour="NULL";

    public function getId(){
    	return $this->id;
    }
    
    public function setId($value){
    	$this->id=$value;
    }
    
    public function getNotes(){
    	if ($this->notes=="NULL"){
    		return $this->notes;
    	}
    	return "'".$this->notes."'";
    }
    
    public function setNotes($value){
    	$this->notes=$value;
    }
    
    public function getTrainer(){
    	return $this->trainer;
    }
    
    public function setTrainer($value){
    	$this->trainer=$value;
    }
  
    public function getUser(){
    	return $this->user;
    }
    
    public function setUser($value){
    	$this->user=$value;
    }
    
    public function setPlan($value){
    	$this->plan=$value;
    }
    public function getPlan(){
    	return $this->plan;
    }
    
    public function setFiliale($value){
    	$this->filiale=$value;
    }
    public function getFiliale(){
    	return $this->filiale;
    }
    
    public function setDate($value){
    	$this->date=$value;
    }
    public function getDate(){
    	return $this->date;
    }
    
    public function setHour($value){
    	$this->hour=$value;
    }
    public function getHour(){
    	return $this->hour;
    }
    
    

   

}
?>