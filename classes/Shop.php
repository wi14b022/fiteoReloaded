<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:
 FITEO has multiple shops, this class contains 
 data for each Shop
 ********************************/


class Shop
{   
	private $id;
    private $email;
    private $phonenumber;
    private $plz;
    private $street;
    private $city;
    private $country;
    
    
    public function setId($id){
    	if (!empty(trim($id))){
    		$this->id=$id;
    		return true;
    	}
    
    	
    	addErrorMessage(ErrorMessage::NoId);
    	return false;
    }
    
    public function getId(){
    	return $this->id;
    }
    
    public function setPhoneNumber($value){
    	
    	$value = ereg_replace("[^0-9]", "", $value);
    	$numberOfDigits = strlen($value);
    	if ($numberOfDigits < 7 or $numberOfDigits > 16) {
    		addErrorMessage('Invalid Phone Number');
    	}
    	
    
    	$this->phonenumber=$value;
    	return TRUE;
    }
    
    public function getPhoneNumber(){
    	return $this->phonenumber;
    }
    
    public function setEmail($value){
    	if (!empty(trim($value))){
    		$this->email=$value;
    		return true;
    	}
    
    	
    	addErrorMessage(ErrorMessage::NoEmail);
    	return false;
    }
    
    public function getEmail(){
    	return $this->email;
    }
    
    
    public function setPlz($value){
    	if (!empty(trim($value))){
    		$this->plz=$value;
    		return true;
    	}
    
    	
    	addErrorMessage(ErrorMessage::NoPlz);
    	return false;
    }
    
    public function getPlz(){
    	return $this->plz;
    }
    
    public function setStreet($value){
    	if (!empty(trim($value))){
    		$this->street=$value;
    		return true;
    	}
    
    	
    	addErrorMessage(ErrorMessage::NoStreet);
    	return false;
    }
    
    public function getStreet(){
    	return $this->street;
    }
    
    public function setCity($value){
    	if (!empty(trim($value))){
    		$this->city=$value;
    		return true;
    	}
    
    	
    	addErrorMessage(ErrorMessage::NoCity);
    	return false;
    }
    
    public function getCity(){
    	return $this->city;
    }
    
    public function setCountry($value){
    	if (!empty(trim($value))){
    		$this->country=$value;
    		return true;
    	}
    
    	
    	addErrorMessage(ErrorMessage::NoCountry);
    	return false;
    }
    
    public function getCountry(){
    	return $this->country;
    }
}
?>