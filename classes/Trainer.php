<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:
 Trainer is a special Employee, in FITEO Web he is chooseable
 in different Views. 
 ********************************/


require_once 'Employee.php';

class Trainer extends Employee
{
    private $special;
    private $trainerDescription;

    public function getSpecial(){
    	return $this->special;
    }
    
    public function setSpecial($value){
    	$this->special=$value;
    }
    
    public function getTrainerDescription(){
    	return $this->trainerDescription;
    }
    
    public function setTrainerDescription($value){
    	$this->trainerDescription=$value;
    }
    

   

}
?>