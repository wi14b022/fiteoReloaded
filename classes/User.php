<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11
 
 Description:
 Basic class for Customer, Employees and Trainer.
 It contains the general data
 ********************************/
class User {
	private $id = 'NULL';
	private $value;
	private $firstName;
	private $lastName;
	private $email;
	private $phonenumber;
	private $login;
	private $password;
	private $plz;
	private $street;
	private $city;
	private $country;
	private $sex;
	
	public function setFirstName($value) {
		$this->firstName = $value;
		return true;
	}
	public function getFirstName() {
		return $this->firstName;
	}
	public function getFullName() {
		return $this->firstName . ' ' . $this->getLastName ();
	}
	public function setLastName($value) {
		$this->lastName = $value;
		return true;

	}
	public function getLastName() {
		return $this->lastName;
	}
	public function setId($id) {
		if (!empty(trim($id)) || $id==0) {
			$this->id = $id;
			return true;
		}
		addErrorMessage ( ErrorMessage::NoId );
		return false;
	}
	public function getId() {
		return $this->id;
	}
	public function setLogin($value) {
		$this->login = $value;
		return true;

	}
	public function getLogin() {
		return $this->login;
	}
	public function setPhoneNumber($value) {
		
		
		$this->phonenumber = $value;
		return TRUE;
	}
	public function getPhoneNumber() {
		return $this->phonenumber;
	}
	public function setEmail($value) {
		$this->email = $value;
		return true;

	}
	public function getEmail() {
		return $this->email;
	}
	public function setPassword($value) {
		$this->password = $value;
		return true;
	}
	public function getPassword() {
		return $this->password;
	}
	public function setPlz($value) {
		$this->plz = $value;
		return true;
	}
	public function getPlz() {
		return $this->plz;
	}
	public function setStreet($value) {
		$this->street = $value;
		return true;
	}
	public function getStreet() {
		return $this->street;
	}
	public function setCity($value) {
		$this->city = $value;
		return true;

	}
	public function getCity() {
		return $this->city;
	}
	public function setCountry($value) {
		$this->country = $value;
		return true;

	}
	public function getCountry() {
		return $this->country;
	}
	public function setSex($value) {
		$this->sex = $value;
		return true;
	}
	public function getSex() {
		return $this->sex;
	}
}
?>