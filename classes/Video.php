<?php
/********************************
 Project:	Case Study 4 - FH Technikum Wien
 Author:	Daniel Perschy
 Date:		2016-04-11

 Description:
 Videos are buyable products
 ********************************/


require_once 'MedienContent.php';

class Video extends MedienContent
{
    private $fitpoints;
    
    public function getFitpoints(){
    	return $this->fitpoints;
    }
    
    public function setFitpoints($value){
    	$this->fitpoints=$value;
    }
    
    public function getPlanType(){
    	return "Video";
    }
    
    public function getBuyLink(){
    	return "type=video&id=".parent::getId();
    }
    
    public function getType(){
    	return "Video";
    }
  

}
?>