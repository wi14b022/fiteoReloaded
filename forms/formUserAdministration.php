<?php 
$disabled_login = '';
$disabled_email = '';
$disabled_password ='';
$enable_blocking_state=FALSE;
$enable_deletion=FALSE;
if (isset($currentUser)) {
	if ($currentUser instanceof Customer || $currentUser instanceof Employee) {
		$disabled_login='disabled=""';
		$disabled_email='disabled=""';
		
	}
	if ($currentUser instanceof Employee) {
		$disabled_password='disabled=""';
		$enable_blocking_state=TRUE;
        $enable_deletion=TRUE;
	}
}

?>
<form class="form-horizontal"
	action="<?php echo $formUserAdministrationAction; ?>" method="post">

	<div class="row">
		<div class="col-md-6">

			<div class="form-group">
				<label for="login" class="col-md-3 control-label">Login name</label>
				<div class="col-sm-9">
					<input id="login" name="login" class="form-control"
						placeholder="Login name"
						value="<?php echo $editUser->getLogin(); ?>" required="required"
						pattern=".{5,20}" required title="5 to 20 characters"
						<?php echo $disabled_login; ?> />
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-md-3 control-label">Email</label>
				<div class="col-sm-9">
					<input id="email" name="email" class="form-control"
						placeholder="Email" value="<?php echo $editUser->getEmail(); ?>"
						required="required" type="email" <?php echo $disabled_email; ?> />
				</div>
			</div>

			<div class="form-group">
				<label for="password" class="col-md-3 control-label">Password</label>
				<div class="col-sm-9">
					<input id="password" name="password" type="password"
						class="form-control" placeholder="Password"
						value="<?php echo $editUser->getPassword(); ?>"
						required="required" pattern=".{5,30}" required
						title="5 to 30 characters" <?php echo $disabled_password; ?> />
				</div>
			</div>


			<div class="form-group">
				<label for="sex" class="col-md-3 control-label">Gender</label>
				<div class="col-md-9">
					<select class="form-control" id="sex" name="sex">
						<option value="M"
							<?php echo selected('M', $editUser->getSex()); ?>>Man</option>
						<option value="W"
							<?php echo selected('W', $editUser->getSex()); ?>>Woman</option>
					</select>
				</div>
			</div>


			<div class="form-group">
				<label for="firstname" class="col-md-3 control-label">First Name</label>
				<div class="col-sm-9">
					<input id="firstname" name="firstname" class="form-control"
						placeholder="First name"
						value="<?php echo $editUser->getFirstName(); ?>"
						required="required" pattern=".{1,50}" required
						title="1 to 50 characters" />
				</div>
			</div>

			<div class="form-group">
				<label for="lastname" class="col-md-3 control-label">Last Name</label>
				<div class="col-sm-9">
					<input id="lastname" name="lastname" class="form-control"
						placeholder="Last name"
						value="<?php echo $editUser->getLastName(); ?>"
						required="required" pattern=".{1,50}" required
						title="1 to 50 characters" />
				</div>
			</div>

		</div>
		<div class="col-md-5">

			<div class="form-group">
				<label for="phonenumber" class="col-md-3 control-label">Phonenumber</label>
				<div class="col-sm-9">
					<input id="phonenumber" name="phonenumber" class="form-control"
						placeholder="Phonenumber"
						value="<?php echo $editUser->getPhoneNumber(); ?>"
						required="required" type="tel" />
				</div>
			</div>


			<div class="form-group">
				<label for="street" class="col-md-3 control-label">Street</label>
				<div class="col-sm-9">
					<input id="street" name="street" class="form-control"
						placeholder="Street" value="<?php echo $editUser->getStreet(); ?>"
						required="required" pattern=".{1,50}" required
						title="1 to 50 characters" />
				</div>
			</div>

			<div class="form-group">
				<label for="city" class="col-md-3 control-label">City</label>
				<div class="col-sm-9">
					<input id="city" name="city" class="form-control"
						placeholder="City" value="<?php echo $editUser->getCity(); ?>"
						required="required" pattern=".{1,50}" required
						title="1 to 50 characters" />
				</div>
			</div>

			<div class="form-group">
				<label for="zip" class="col-md-3 control-label">Zip-Code</label>
				<div class="col-sm-9">
					<input id="zip" name="zip" class="form-control"
						placeholder="Zip-Code" value="<?php echo $editUser->getPlz(); ?>"
						required="required" type="number" min="1000" max="99999" required
						title="1 to 50 characters" />
				</div>
			</div>

			<div class="form-group">
				<label for="country" class="col-md-3 control-label">Country</label>
				<div class="col-sm-9">
					<select class="form-control" id="country" name="country">
						<option <?php echo selected('Bicycling', ""); ?>>Austria</option>
					</select>
				</div>
			</div>

		</div>


	</div>
	<div class="row">
		<div class="col-md-6">
	<?php if(isset($currentUser)) { 
	if($currentUser instanceof Customer){
		
	
		?>
		<div class="form-group">
				<label for="size" class="col-md-3 control-label">Size</label>
				<div class="col-sm-9">
					<input id="size" name="size" class="form-control"
						placeholder="Size" type="number"
						value="<?php echo $editUser->getSize(); ?>" />
				</div>
			</div>

			<div class="form-group">
				<label for="diet" class="col-md-3 control-label">Diet</label>
				<div class="col-sm-9">
					<input id="diet" name="diet" class="form-control"
						placeholder="Diet" value="<?php echo $editUser->getDiet(); ?>" />
				</div>
			</div>
		</div>
		<div class="col-md-5">

			<div class="form-group">
				<label for="lifestyle" class="col-md-3 control-label">Lifestyle</label>
				<div class="col-sm-9">
					<input id="lifestyle" name="lifestyle" class="form-control"
						placeholder="Lifestyle"
						value="<?php echo $editUser->getLifestyle(); ?>" />
				</div>
			</div>

			<div class="form-group">
				<label for="trainer" class="col-md-3 control-label">Trainer</label>
				<div class="col-sm-9">
					<select class="form-control" id="trainer" name="trainer">
						<option <?php echo selected($editUser->getTrainer(), ""); ?>
							value="null">none</option>
											<?php foreach ($trainersWithoutDummy as $trainer) {?>
											<option
							<?php echo selected($editUser->getTrainer(), $trainer->getId()); ?>
							value="<?php echo $trainer->getId();?>"><?php echo $trainer->getFullName(); ?></option>
											<?php } ?>
										</select>
				</div>
			</div>
		</div>
		
	<?php }}?>
</div>

	<div class="row">
		<div class="col-md-11 align-right">
			<div class="form-group">
				<div class=" text-right">
					<input type="hidden" id="id" name="id"
						value="<?php echo $editUser->getId(); ?>" /> <input type="hidden"
						id="save" name="save" value="create" />
                    <?php if ($enable_deletion) { ?>
                        <a href="?site=AdministrateUserEdit&edit=<?php echo $editUser->getId(); ?>&delete=1"
                           class="btn btn-default">Delete User</a>
                    <?php } ?>
					<?php if ($enable_blocking_state) { ?>
					<a href="?site=AdministrateUserEdit&edit=<?php echo $editUser->getId(); ?>&status=1"
						class="btn btn-default"><?php echo substr($editUser->getStatusReadableInverse(), 0, -2); ?></a>
					<?php } ?>
					<button type="submit" class="btn btn-default">Save Data</button>
				</div>
			</div>
		</div>



	</div>
</form>
