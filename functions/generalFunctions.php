<?php

function addErrorMessage($message){
	global $errorMessage;
	$errorMessage.=$message.'<br>';
}

function addSuccessMessage($message){
	global $successMessage;
	$successMessage.=$message.'<br>';
}


function logging($message){
	global $messages;
	$messages.=$message.'<br>';
	$file = 'log.txt';
	file_put_contents($file, $message.PHP_EOL, FILE_APPEND);
}

function selected($a, $b){
	if ($a==$b) return "selected";
}
function ifSet($a){
	if (isset($a)) echo $a;
	else echo "";
}

function cast($destination, $sourceObject)
{
	if (is_string($destination)) {
		$destination = new $destination();
	}
	$sourceReflection = new ReflectionObject($sourceObject);
	$destinationReflection = new ReflectionObject($destination);
	$sourceProperties = $sourceReflection->getProperties();
	foreach ($sourceProperties as $sourceProperty) {
		$sourceProperty->setAccessible(true);
		$name = $sourceProperty->getName();
		$value = $sourceProperty->getValue($sourceObject);
		if ($destinationReflection->hasProperty($name)) {
			$propDest = $destinationReflection->getProperty($name);
			$propDest->setAccessible(true);
			$propDest->setValue($destination,$value);
		} else {
			$destination->$name = $value;
		}
	}
	return $destination;
}