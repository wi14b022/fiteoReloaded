<?php 
	$messages="";	
	require_once 'classes/User.php';
	require_once 'classes/Customer.php';
	require_once 'classes/Employee.php';
	require_once 'classes/ETR.php';
	require_once 'classes/DB.php';
	require_once 'classes/Shop.php';
	require_once 'classes/Request.php';
	require_once 'languages/en/LockingState.php';
	require_once 'languages/en/ErrorMessage.php';
	require_once 'languages/en/SuccessMessage.php';
	require_once 'languages/en/GeneralMessage.php';
	require_once 'functions/generalFunctions.php';		
	session_start();
	
	if (isset($_GET['logout'])) {
		unset($currentUser);
		unset($_SESSION['currentUser']);
		session_destroy();
	}
	if(!empty($_SESSION['currentUser']))
	{
		$currentUser=$_SESSION['currentUser'];
		$editUser = $currentUser;
		logging("UserId:".$currentUser->getId());
		$formUserAdministrationAction="?site=EditUser";
		
	}
	//else $currentUser=new User();
	
	//require_once 'functions/createDummyCustomer.php';	
	$success="";
	global $errorMessage;
	$errorMessage="";
	$title="FITEO";
	
	$jsScripts=array();
	$db = new DB();
	$db->setConnection();
	$trainersWithoutDummy = $db->getTrainerArray();
	unset($trainersWithoutDummy[0]);
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="images/favicon.png">


    <!--[if lt IE 9]>
		<script src="js/html5/html5shiv.min.js"></script>
		<script src="js/html5/respond.min.js"></script>
    <![endif]-->

	<!-- Bootstrap core CSS -->
    <link href="libraries/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="libraries/header-1/flexslider.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="css/media.css" rel="stylesheet">	

	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	
		<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	
  
  	<?php 
    if (isset($_GET['site']))
    	include('logic/logic'.$_GET['site'].'.php'); 
    
    //logging("UserId:".$currentUser->getId());
    
    if (isset($currentUser)){
    	if ($currentUser instanceof Customer){
    		if ($currentUser->getStatus()=="B"){
    			addErrorMessage("Customer is currently blocked");
    		}
    	}
    }

    
    ?>
    
 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $title; ?></title>
    
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
   
	<script src="js/Chart.js/Chart.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    
   	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <!-- CDN Datatables -->
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script> 
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" >

	<!-- Datepicker -->
    <script src="plugins/datepicker/js/bootstrap-datepicker.js"></script>
    <link id="bsdp-css" href="plugins/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">

	<!-- Libraries -->
	<script src="libraries/modernizr.custom.js"></script>
	<script src="libraries/jquery.easing.min.js"></script><!-- Easing Animation Effect -->
	<script src="libraries/wow.min.js"></script> <!-- WOW Animation Effect -->
	<script src="libraries/owl-carousel/owl.carousel.min.js"></script> <!-- OWL Carousel -->
	<script src="libraries/header-1/jquery.flexslider.js"></script> <!-- Flex Photos Slider -->
	<script src="libraries/responsive-calendar.js"></script> <!-- Responsive Calendar -->
	<script src="libraries/jquery.animateNumber.min.js"></script> <!-- Used for Animated Numbers -->
	<script src="libraries/jquery.appear.js"></script> <!-- It Loads jQuery when element is appears -->
	<script src="libraries/jquery.knob.js"></script> <!-- Used for Loading Circle -->
	<script src="libraries/page-preloading/classie.js"></script>
	<script src="libraries/page-preloading/pathLoader.js"></script>
	<script src="libraries/page-preloading/main.js"></script>

    
	<?php 
    foreach ($jsScripts as $script){
    	echo '<script src="js/'.$script.'"></script>';
    } 
    	
    ?>

</head>
<body data-offset="200" data-spy="scroll" data-target=".navbar-main">
	<a id="top"></a>
	<div id="ip-container" class="ip-container">
		<!-- initial header -->
		

		<!-- Header 1 -->
		<header class="header-4">
			<div class="header-4-inner">
				<div class="container">
					<div class="logo-left col-md-3 col-sm-3 col-xs-12">
						<a href="index.php"><img src="images/header/header-1/logo.png" alt="hermes" title="hermes" /></a>
					</div>
					<nav class="menu-right navbar-main primary-navigation col-md-9">
						<div class="social-icons ">
                            <div class="hidden-xs">
                                <?php include('forms/formLogin.php');  ?>
                                <hr>
                            </div>


						</div>
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-right">
                                <div class="login visible-xs">
                                    <?php include('forms/formLogin.php');  ?>
                                </div>
								<?php if (isset($currentUser)) 
					            {
					            if ($currentUser instanceof Customer) 
					            {
					            	logging($currentUser->getStatus());
					            	if ($currentUser->getStatus()=="U"){
					            		
					            ?>


					            		<li><a href="?site=Fitpoints">buy Fitpoints</a></li>
							            <li><a href="?site=BookCourse">get Courses</a></li>
							            <li><a href="?site=Products">get Products</a></li>
							            <li><a href="?site=Fitness">My Fitness</a></li>
							            <li class="dropdown blog-dropdown">
											<a href="" class="dropdown-toggle" data-toggle="dropdown">request Trainer</a>
											<ul class="dropdown-menu" role="menu">
												<li><a href="?site=requestMeeting">request Meeting</a></li>
												<li><a href="?site=changePlan">request change Plan</a></li>
											</ul>
										</li>
										<li class="dropdown blog-dropdown">
											<a href="" class="dropdown-toggle" data-toggle="dropdown">check ETR</a>
											<ul class="dropdown-menu" role="menu">
												<li><a href="?site=ETR">my ETR</a></li>
												<li><a href="?site=TrainerETR">Trainer ETR</a></li>
											</ul>
										</li>
					            <?php }
					            }
					            else if ($currentUser instanceof Employee) 
					            {
					            	?>
					                        <li><a href="?site=AdministrateUser">User Administration</a></li>
					            <?php }          
					            } 
					            else {
					            ?>
                                <li><a href="#">why us</a></li>
								<li><a href="#upcoming-courses">our CLASSES</a></li>
								<li><a href="#programs-timeline">popular programs</a></li>					
								<li><a href="#scheduler-calendar">see schedule</a></li>
								<li><a href="#our-trainer">the TRAINERS</a></li>
								<li><a href="#contact">the CONTACT</a></li>
								<?php 
					            }
								?>
								
							</ul><!--/.nav-right -->
						</div><!--/.nav-collapse -->
					</nav>
				</div>
			</div>
		</header><!-- /.container -->

		

		
    
    <?php if(!empty($successMessage)){?>
	<div class="alert alert-success"><?php echo $successMessage; ?></div>
	<?php } ?>
	<?php if(!empty($errorMessage)){?>
	<div class="alert alert-danger"><?php echo $errorMessage; ?></div>
	<?php } ?>
	    
    <?php 
    if (isset($view)){
    	include('views/'.$view);
    }
    else include('views/viewNotRegistered.php');
    	
    ?>
    <?php if (isset($currentUser)) { ?>
		<?php if ($currentUser instanceof Customer) { ?>	
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog modal-lg">
			
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Change Customer data</h4>
						</div>
						<div class="modal-body">
							<?php include('forms/formUserAdministration.php'); ?>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
			
				</div>
			</div>
		<?php } ?>
	<?php } ?>

    

    <?php 
    if(isset($currentUser))
    {
    	if (empty($currentUser->getId())){
    		logging("Current user is empty");
    		//$currentUser=null;
    	}
    	$db->getUserById($currentUser);
    	$_SESSION['currentUser']=$currentUser;
    	logging("Save User into Session");
    	
    }
    else logging("Not Save User");
    
    
    
    ?>

		<footer class="footer-section ow-fluid-section container-fluid">
			<div class="footer-logo-section container">
				<a href="index.html"><img src="images/header/header-1/logo.png" class="footer-logo" alt="footer logo"/></a>
				<ul class="footer-social">
					<li><a href="#"><img src="images/footer/footer-1/facebook.png" alt="facebook icon"></a></li>
					<li><a href="#"><img src="images/footer/footer-1/flickr.png" alt="flickr icon"></a></li>
					<li><a href="#"><img src="images/footer/footer-1/google.png" alt="google icon"></a></li>
					<li><a href="#"><img src="images/footer/footer-1/dribble.png" alt="dribble icon"></a></li>
					<li><a href="#"><img src="images/footer/footer-1/twitter.png" alt="twitter icon"></a></li>
				</ul>
			</div>
			<div class="ow-fixed-section container">
				<div class="ow-fixed-section-inner">
					<div class="col-md-3 col-sm-6">
						<aside class="widget widget_faqs">
							<i class="widget_icon"></i>
							<h2 class="widget-title"> Customer care </h2>
							<span class="widget-sub-title"> faq and answers </span>
							<div class="panel-group faq-accordion" id="faq-accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#faq-accordion" href="#faq-1"> How do i subscribe? </a>
										</h4>
									</div>
									<div id="faq-1" class="panel-collapse collapse in">
										<div class="panel-body">
											This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#faq-accordion" href="#faq-2"> How do i pay? </a>
										</h4>
									</div>
									<div id="faq-2" class="panel-collapse collapse">
										<div class="panel-body">
											This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#faq-accordion" href="#faq-3"> Cancelling subscriptions </a>
										</h4>
									</div>
									<div id="faq-3" class="panel-collapse collapse">
										<div class="panel-body">
											This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#faq-accordion" href="#faq-4"> Do you offer counseling? </a>
										</h4>
									</div>
									<div id="faq-4" class="panel-collapse collapse">
										<div class="panel-body">
											This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#faq-accordion" href="#faq-5"> Refund policy </a>
										</h4>
									</div>
									<div id="faq-5" class="panel-collapse collapse">
										<div class="panel-body">
											This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum
										</div>
									</div>
								</div>
							</div>
						</aside>
					</div>
					<div class="col-md-3 col-sm-6">
						<aside class="widget widget_special_offers">
							<i class="widget_icon"></i>
							<h2 class="widget-title"> special offers! </h2>
							<span class="widget-sub-title"> this month�s sweet deals </span>
							<div id="myCarousel" data-ride="carousel" class="carousel slide special-offers">  
								<div class="carousel-inner">
									<div class="item active">
										<a href="#"><img src="images/footer/footer-1/special-offers.png" alt="Special Offers"/></a>
										<div class="slide-content">
											<div class="offer-price"> <b>15</b>%<span> Off</span> </div>
											<a href="#"><h4 class="offer-title"> modern dance classes </h4></a>
										</div>
									</div>
									<div class="item">
										<a href="#"><img src="images/footer/footer-1/special-offers.png" alt="Special Offers"/></a>
										<div class="slide-content">
											<div class="offer-price"> <b>15</b>%<span> Off</span> </div>
											<a href="#"><h4 class="offer-title"> modern dance classes </h4></a>
										</div>
									</div>
								</div>
								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</aside>
					</div>
					<div class="col-md-3 col-sm-6">
						<aside class="widget widget_special_offers">
							<i class="widget_icon"></i>
							<h2 class="widget-title"> special offers! </h2>
							<span class="widget-sub-title"> this month�s sweet deals </span>
							<div id="myCarousel" data-ride="carousel" class="carousel slide special-offers">  
								<div class="carousel-inner">
									<div class="item active">
										<a href="#"><img src="images/footer/footer-1/special-offers.png" alt="Special Offers"/></a>
										<div class="slide-content">
											<div class="offer-price"> <b>15</b>%<span> Off</span> </div>
											<a href="#"><h4 class="offer-title"> modern dance classes </h4></a>
										</div>
									</div>
									<div class="item">
										<a href="#"><img src="images/footer/footer-1/special-offers.png" alt="Special Offers"/></a>
										<div class="slide-content">
											<div class="offer-price"> <b>15</b>%<span> Off</span> </div>
											<a href="#"><h4 class="offer-title"> modern dance classes </h4></a>
										</div>
									</div>
								</div>
								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</aside>
					</div>
					<div class="col-md-3 col-sm-6">
						<aside class="widget widget_mc4wp_widget">
							<i class="widget_icon"></i>
							<h2 class="widget-title"> subscribe to newsletter </h2>
							<span class="widget-sub-title"> find out before anyone else! </span>
							<form id="mc4wp-form-1" class="form mc4wp-form" action="#" method="post">
								<p>
									<label class="sr-only" for="mc4wp_email">Email address: </label>
									<input id="mc4wp_email" type="email" required="" placeholder="What�s your e-mail?" name="EMAIL">
								</p>
								<p><input type="submit" class="btn btn-hermes" value="Send!"></p>
							</form>
						</aside>
						<aside class="widget widget_workinghours">
							<i class="widget_icon"></i>
							<h2 class="widget-title"> working hours </h2>
							<span class="widget-sub-title"> when you can find us </span>
							<div class="textwidget">
								<div class="col-md-12 col-sm-6 col-lg-6">
									<h4> Fitness & gym: </h4>
									<p><b>Mon - Fri</b> : 08:00 - 21:00</p>
									<p><b>sat - sub</b> : 10:00 - 21:00</p>
								</div>
								<div class="col-md-12 col-sm-6 col-lg-6">
									<h4> Dance studio: </h4>
									<p><b>Mon - Fri</b> : 08:00 - 21:00</p>
									<p><b>sat - sub</b> : 10:00 - 21:00</p>
								</div>
							</div>
						</aside>
					</div>
				</div>
			</div><!-- /.container -->		
			<a href="javascript:" id="back-to-top"><i class="fa fa-arrow-up"></i></a>
			<div class="ow-fixed-section copyright-notice container">
				Copyright &copy; Fiteo 2016. Designed with care, crafted with love.
			</div><!-- /.container -->
		</footer><!-- /.container-fluid -->
	</div>

  <script src="js/hermes.js"></script>
	
</body>
</html>