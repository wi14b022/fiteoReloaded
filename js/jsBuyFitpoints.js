
$(document).ready(function() {
	$("#approved").hide();
	$("#executing").hide();  
	
	$("#paypal").submit(function(e)
	{
		var form = $('#paypal');
		var serializedData = form.serialize();
		
		
		
		$("#executing").text("Payment is in execution, please Wait...");
		$("#executing").show();
		$("#paypal :input").attr("disabled", true);
		$("#sendButton").attr("disabled", true);
		
		$.ajax({
		    url : "ajax/ajaxPaypalPayment.php",
		    type: 'POST',
		    data: serializedData,
		    success: function(data, textStatus, jqXHR)
		    {
		        if (data.trim()=="approved"){
		        	$("#approved").text("Payment was approved, fitpoints were added");
		        	$("#approved").show();
		        	$("#executing").hide();
		        }
		        else {
		        	$("#executing").html(data);
		        	$("#executing").show();
		        	$("#approved").hide();
		        	$("#paypal :input").attr("disabled", false);
		    		$("#sendButton").attr("disabled", false);
		        }
		    },
		    error: function (jqXHR, textStatus, errorThrown)
		    {
		    	$("#executing").text("Service not reachable:" + textStatus );
		    	$("#executing").show();
	        	$("#approved").hide();
	        	$("#paypal :input").attr("disabled", false);
	    		$("#sendButton").attr("disabled", false);
		    },
		});
			    e.preventDefault(); //STOP default action
			    e.unbind(); //unbind. to stop multiple form submit.
	});
			 
			
    
});
function addContact () {
	$("#paypal").submit(); //Submit  the FORM
}
