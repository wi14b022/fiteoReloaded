
    	$( document ).ready( function() {
    		
    		function getUrlVars()
    		{
    		    var vars = [], hash;
    		    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    		    for(var i = 0; i < hashes.length; i++)
    		    {
    		        hash = hashes[i].split('=');
    		        vars.push(hash[0]);
    		        vars[hash[0]] = hash[1];
    		    }
    		    return vars;
    		}

    		var serverEvents = 
			{
			   
			}

    		var today = new Date();
    		var dd = today.getDate();
    		var mm = today.getMonth()+1; //January is 0!
    		var yyyy = today.getFullYear();

    		if(dd<10) {
    		    dd='0'+dd
    		} 

    		if(mm<10) {
    		    mm='0'+mm
    		} 

    		today = yyyy+'-'+mm+'-'+dd;

    		var day=today
	    	if (getUrlVars()["date"]){
	    		day=getUrlVars()["date"];
	    		//changeCalendar(day);
	    	}
			

    		$(".responsive-calendar").responsiveCalendar(
    				{
    					
    					//time: '2016-05-01',
    					events: serverEvents,
    					startFromSunday : 0,
    					activateNonCurrentMonths: 0,

    					onDayClick: function(events) { 
            		    	var thisDayEvent, key;

            		    	key = $(this).data('year')+'-'+ addLeadingZero($(this).data('month')) +'-'+addLeadingZero($(this).data('day'))
            		    	thisDayEvent = events[key];
            	    	    //alert(key);
            	    	    day=key;
            	    	    changeCalendar(key);
            	    	    getCalendar();
                		    }
    					
    				});


        	
    		function addLeadingZero(num) {
    		    if (num < 10) {
    		      return "0" + num;
    		    } else {
    		      return "" + num;
    		    }
    		  }


    		function changeCalendar(appointment) {
				var str = '{"' + appointment + '":{"class":"active"}}';
				var data=JSON.parse(str);
				$('.responsive-calendar').responsiveCalendar('clearAll')
    			$('.responsive-calendar').responsiveCalendar('edit', data)
        	}

    		$( "#trainer" ).change(function() {
    			getCalendar();
    		});

    		$( "#location" ).change(function() {
    			getCalendar();
    		});

    		function getCalendar(){

    			
    			data = "trainer=" + $("#trainer").val() + "&location=" + $("#location").val() + "&day="+day;  
    			
    			//alert(data);

    			
    			
    			
	    		$.ajax({
	    		    url : "ajax/ajaxRequestMeeting.php",
	    		    type: 'POST',
	    		    data: data,
	    		    success: function(data, textStatus, jqXHR)
	    		    {
	    		       $("#events").html(data);
	    		    },
	    		    error: function (jqXHR, textStatus, errorThrown)
	    		    {
	    		    	
	    		    },
	    		});

    		}

    		getCalendar();
    		
    		
  		     		  
    	});