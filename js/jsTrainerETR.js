/**
 * 
 */
$(document).ready(function() {
	
	
	var buyerData = {
            labels : muscleMassLabels,
            datasets : [
            {
	            label: "Apodis",
	            fillColor: "rgba(220,220,220,0.2)",
	            strokeColor: "rgba(220,220,220,1)",
	            pointColor: "rgba(220,220,220,1)",
	            pointStrokeColor: "#fff",
	            pointHighlightFill: "#fff",
	            pointHighlightStroke: "rgba(220,220,220,1)",
	            data: muscleMass
	        }
        ]
        }
        // get line chart canvas
        var MuscleMass = document.getElementById('muscleMass').getContext('2d');
        Chart.defaults.global.responsive = true;
        // draw line chart
        new Chart(MuscleMass).Line(buyerData).options;
        // pie chart data

        
        
        var ApodisData = {
                labels : adiposeLabels,
                responsive: true,
                datasets : [
                {
		            label: "Muscle mass",
		            fillColor: "rgba(151,187,205,0.2)",
		            strokeColor: "rgba(151,187,205,1)",
		            pointColor: "rgba(151,187,205,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: adipose
		        }
            ]
            }
            // get line chart canvas
            var Apodis = document.getElementById('apodis').getContext('2d');
            // draw line chart
            new Chart(Apodis).Line(ApodisData);
            // pie chart data
       
            
            var PulseData = {
                    labels : pulseLabels,
                    datasets : [
                    {
    		            label: "Pulse",
    		            fillColor: "rgba(151,187,205,0.2)",
    		            strokeColor: "rgba(151,187,205,1)",
    		            pointColor: "rgba(151,187,205,1)",
    		            pointStrokeColor: "#fff",
    		            pointHighlightFill: "#fff",
    		            pointHighlightStroke: "rgba(151,187,205,1)",
    		            data: pulse
    		        }
                ]
                }
                // get line chart canvas
                var Pulse = document.getElementById('pulse').getContext('2d');
                // draw line chart
                new Chart(Pulse).Line(PulseData);
                // pie chart data 
        
});