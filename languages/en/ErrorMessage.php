<?php 
class ErrorMessage {
	const __default = self::Error;
	
	const MYSQL_1062 = "Email or Login already existent";

	const NoDate = "No date was given";
	
	
	//User
	const NoFirstName = "No workout was given"; 
	const NoLastName = "No last name was given";
	const NoId = "No Id was given";
	const NoLogin = "No Login was given";
    const LoginAlreadyInUse = "Login already existing";
	const NoPhoneNumber = "No Phone was given";
	const NoEmail = "No Email was given";
    const EmailAlreadyInUse = "Email already existing";
	const NoPassword = "No Password was given";
	const NoPlz = "No ZIP was given";
	const NoStreet = "No Street was given";
	const NoCity = "No City was given";
	const NoCountry = "No Country was given";
	const NoSex = "No sex was given";

	
	///ETR
	const NoWorkout = "No workout was given";
	const NoDuration = "No duration was given";
	
	const Error = "Unexpected Error<br/>";
}
?>