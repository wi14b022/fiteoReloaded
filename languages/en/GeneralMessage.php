<?php 
class GeneralMessage {
	const __default = self::def;

	
	const Edit = "Edit";
	const Save = "Save";
	const Hours = "Hours";
	const Date = "Date";
	const Change = "Change";
	
	//Customer
	const FirstName = "First Name";
	const LastName = "Last Name";
	const Email = "Email";
	const Login = "Login";
	const City = "City";
	const LockingState = "Locking State";
	
	//ETR specific
	const Locked = "Lock";
	const Unlocked = "Unlock";
	const Workout = "Workout";

	
	const def = "Text nicht gefunden";
}
?>