<?php
class LockingState {
	const __default = self::UNKNOWN;

	const BLOCKED = "Blocked";
	const UNBLOCKED = "Unblocked";
	const UNKNOWN = "Unknown";

}