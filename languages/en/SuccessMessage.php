<?php 
class SuccessMessage {
	const __default = self::Success;

	const Success = "Success";
	
	//ETR specific
	const EtrCreated = "ETR was succesfully created";
	const EtrUpdated = "ETR was succesfully updated";
	const EtrLockingStateUpdated = "Locking of the ETR was successfuly updated";
	
}
?>