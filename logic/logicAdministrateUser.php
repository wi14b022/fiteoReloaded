<?php
$view = "viewAdministrateUser.php";
array_push($jsScripts, "jsDataTable.js");

if (isset($_GET['delete']))
{
    $editUser = new Customer();
    $editUser->setId($_GET['delete']);
    global $errorMessage;
    if(empty($errorMessage)){
        $db->deleteUser($editUser);
        addSuccessMessage("User was successfully deleted");
    }

}

$customers = $db->getCustomers();

