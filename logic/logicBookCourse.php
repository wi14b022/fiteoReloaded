<?php
$view = "viewBookCourse.php";
array_push($jsScripts, "jsBookCourse.js");

if ($_GET){
	$request = new Request();
	if (isset($_GET['location'])){
		$request->setFiliale($_GET['location']);
	}
	if (isset($_GET['course']) && isset($_GET['date']) && isset($_GET['hour'])){
		$course = new Course();
		$course->setId($_GET['course']);
		$course->setDate($_GET['date']);
		$course->setHour($_GET['hour']);
		$db->setCourse($currentUser, $course);
		
	}	
}

$shops = $db->getShopArray();