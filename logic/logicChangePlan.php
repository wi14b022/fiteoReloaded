<?php
$view = "viewChangePlan.php";
array_push($jsScripts, "jsChangePlan.js");
$trainers = $db->getTrainerArray();
$disabled='disabled=""';
$editplan=new Plan();

if ($_GET){
	if (isset($_GET['plan'])){
		$plan = new Plan();
		$plan->setId($_GET['plan']);
		if (isset($currentUser->getUnlockedProducts()[$_GET['plan']])){
			if($currentUser->getUnlockedProducts()[$_GET['plan']] instanceof Plan){
				$editplan=$currentUser->getUnlockedProducts()[$_GET['plan']];
				$disabled='';
			}
			else addErrorMessage("Video plans cannot be changed");
		}
		else addErrorMessage("Plan doesnt exists");		
	}
}

if ($_POST){
	
	if (isset($_POST['trainer'])){
		$request = new Request();
		$trainer = new Trainer();
		$plan = new Plan();
		
		$trainer->setId($_POST['trainer']);
		$db->getTrainer($trainer);
		$plan->setId($_POST['plan']);
		$db->getPlanById($plan);

		$request->setTrainer($trainer);
		$request->setPlan($plan);
		if(isset($_POST['notes'])){
			$request->setNotes($_POST['notes']);
		}
		
		
		$request->setUser($currentUser);
		$db->setChangePlanRequest($request);
	}
}