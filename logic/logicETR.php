<?php
array_push($jsScripts, "jsETR.js");
$view = "viewETR.php";

$id="";
if (isset($_GET['edit'])){
	$id=$_GET['edit'];
}

if (isset($_GET['edit']))
{
	$editEtr=$currentUser->getUserETRs()[$_GET['edit']-1];
	
}

if (isset($_POST['save']))
{

	if(!empty($_POST['id']))
	{

		$editEtr=$currentUser->getUserETRs()[$_POST['id']-1];
		$editEtr->setDuration($_POST['duration']);
		$editEtr->setRecordType($_POST['workout']);
        $editEtr->setTrainer($_POST['trainer']);
		$db->updateEtr($editEtr);
		$editEtr=$currentUser->getUserETRs()[$_POST['id']-1];

	}
	else
	{
		$editEtr = new ETR();
		$editEtr->setRecorder('C');
		$editEtr->setDuration($_POST['duration']);
		$editEtr->setRecordType($_POST['workout']);
        $editEtr->setTrainer($_POST['trainer']);
		$db->insertETR($editEtr, $currentUser);
	}

}

if (isset($_GET['changeLock']))
{
	if(!empty($_GET['changeLock']))
	{
		$editEtr=$currentUser->getUserETRs()[$_GET['changeLock']-1];
		
		if($editEtr->getTrainer()=="NULL"){
			if (!empty($currentUser->getTrainer())){
				$editEtr->setTrainer($currentUser->getTrainer());
				$db->updateEtr($editEtr);
			}
			else addErrorMessage("You have no default Trainer");
		}
		else {
			$editEtr->setTrainer("NULL");
			$db->updateEtr($editEtr);
		}
		
		
		
		logging("ChangeLock");
	}

}

if (!isset($editEtr)) $editEtr = new ETR();

if (empty($id)){
	$saveText="Create new Training Record";
}
else $saveText="Update Training Record";

$db->getUserById($currentUser);
?>