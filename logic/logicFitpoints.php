<?php
require_once 'classes/FitpointPackage.php';
require_once 'classes/DB.php';
$view = "viewFitpoints.php";
array_push($jsScripts, "jsBuyFitpoints.js");
$fitpointPackages = array();
$fitpointPackages=$db->getFitpointsPackages();

if(empty($fitpointPackages)){
    addErrorMessage("No Fitpoints packages found");
    $view = "viewError.php";
}

?>
