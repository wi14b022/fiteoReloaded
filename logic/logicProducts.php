<?php
array_push($jsScripts, "jsFormUserAdministration.js");
$view = "viewProducts.php";
$editUser = $currentUser;
$trainers = $db->getTrainerArray();

/*
 * Change Customer Data
 */
if ($_POST)
{
	if (isset($_POST['saveCustomerData'])){
		$currentUser->setLifestyle($_POST['lifestyle']);
		$currentUser->setDiet($_POST['diet']);
		$currentUser->setSize($_POST['size']);
		$currentUser->setTrainer($_POST['trainer']);
		
		$db->updateCustomer($currentUser);
		
	}
	

}

if(isset($_GET['id'])){
	if ($_GET['type']=='plan'){
		$product=new PredefinedPlan();
	}
	else if ($_GET['type']=='video'){
		$product=new Video();
	}
	$product->setId($_GET['id']);
	$db->buyProduct($product, $currentUser);
}

/*
 * Get buyable products
 */

$buyableProducts=$db->getBuyableProductsArray($currentUser);

?>