<?php
array_push($jsScripts, "jsFormUserAdministration.js");
$view = "viewRegister.php";
$formUserAdministrationAction="";
$editUser = new User();

if ($_POST)
{
	$currentCustomer=null;
	$currentCustomer=new Customer();
	
	if (empty(trim($_POST['city']))){
		addErrorMessage(ErrorMessage::NoCity);
	}
	else $currentCustomer->setCity($_POST['city']);
	
	if (empty(trim($_POST['country']))){
		addErrorMessage(ErrorMessage::NoCountry);
	}
	else $currentCustomer->setCountry($_POST['country']);
	
	if (empty(trim($_POST['email']))){
		addErrorMessage(ErrorMessage::NoEmail);
	}
	else if (!$db->checkEmailIsNotUsed($_POST['email']))
    {
        addErrorMessage(ErrorMessage::EmailAlreadyInUse);
    }
	else $currentCustomer->setEmail($_POST['email']);
	
	if (empty(trim($_POST['firstname']))){
		addErrorMessage(ErrorMessage::NoFirstName);
	}
	else $currentCustomer->setFirstName($_POST['firstname']);
	
	if (empty(trim($_POST['lastname']))){
		addErrorMessage(ErrorMessage::NoLastName);
	}
	else $currentCustomer->setLastName($_POST['lastname']);
	
	if (empty(trim($_POST['login']))){
		addErrorMessage(ErrorMessage::NoLogin);
	}
    else if (!$db->checkUserNameIsNotUsed($_POST['login']))
    {
        addErrorMessage(ErrorMessage::LoginAlreadyInUse);
    }
	else $currentCustomer->setLogin($_POST['login']);
	
	if (empty(trim($_POST['password']))){
		addErrorMessage(ErrorMessage::NoPassword);
	}
	else $currentCustomer->setPassword($_POST['password']);
	
	if (empty(trim($_POST['phonenumber']))){
		addErrorMessage(ErrorMessage::NoPlz);
	}
	else {
		$value = ereg_replace ( "[^0-9]", "", $_POST['phonenumber'] );
		$numberOfDigits = strlen ( $_POST['phonenumber'] );
		if ($numberOfDigits < 7 or $numberOfDigits > 16) {
			addErrorMessage ( 'Invalid Phone Number' );
		}
		else $currentCustomer->setPhoneNumber($_POST['phonenumber']);
	}
	
	if (empty(trim($_POST['zip']))){
		addErrorMessage(ErrorMessage::NoPlz);
	}
	else $currentCustomer->setPlz($_POST['zip']);
	
	if (empty(trim($_POST['sex']))){
		addErrorMessage(ErrorMessage::NoSex);
	}
	else $currentCustomer->setSex($_POST['sex']);
	
	if (empty(trim($_POST['street']))){
		addErrorMessage(ErrorMessage::NoStreet);
	}
	else $currentCustomer->setStreet($_POST['street']);
	
	
	$editUser=$currentCustomer;
	//$editUser->setPassword("");
	
	global $errorMessage;
	if(empty($errorMessage)){
		$db->saveCustomer($currentCustomer);
	}
	if(empty($errorMessage)) {
		$view = "viewEmpty.php";
	}
	
}
?>