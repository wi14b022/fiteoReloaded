<?php
/********************************
Project: Case Study 4 - FH Technikum Wien
Author: Daniel Perschy

Description: Creates for each ETR which is created by 
an Trainer JavaScript Variables for jsTrainerETR.js
********************************/

array_push($jsScripts, "jsTrainerETR.js");
$view = "viewTrainerETR.php";
$showGraphics=true;

$adipose='[';
$adiposeLabels='[';
$adiposeCount=1;

$muscleMass='[';
$muscleMassLabels='[';
$muscleMassCount=1;

$pulse='[';
$pulseLabels='[';
$pulseCount=1;

if(empty($currentUser->getTrainerETRs())){
    addErrorMessage("No Measurement ETRs found, please request a Meeting with a Trainer");
    $showGraphics=false;
}

foreach ($currentUser->getTrainerETRs() as $etr){
	if ($etr->getAdipose()!="NULL" || !empty($etr->getAdipose())){
		$adipose.=$etr->getAdipose().', ';
		$adiposeLabels.='"'.$adiposeCount.'",';
		$adiposeCount++;
	}
	
	if ($etr->getMuscleMass()!="NULL" || !empty($etr->getMuscleMass())){
		$muscleMass.=$etr->getMuscleMass().', ';
		$muscleMassLabels.='"'.$muscleMassCount.'",';
		$muscleMassCount++;
	}
	
	if ($etr->getPuls()!="NULL" || !empty($etr->getPuls())){
		$pulse.=$etr->getPuls().', ';
		$pulseLabels.='"'.$pulseCount.'",';
		$pulseCount++;
	}

}
$adipose=rtrim($adipose, ', ').']';
$adiposeLabels=rtrim($adiposeLabels, ', ').']';

$muscleMass=rtrim($muscleMass, ', ').']';
$muscleMassLabels=rtrim($muscleMassLabels, ', ').']';

$pulse=rtrim($pulse, ', ').']';
$pulseLabels=rtrim($pulseLabels, ', ').']';

?>

<script type="text/javascript">
var adipose = <?php echo $adipose; ?>

var adiposeLabels = <?php echo $adiposeLabels; ?>

var muscleMass = <?php echo $muscleMass; ?>

var muscleMassLabels = <?php echo $muscleMassLabels; ?>

var pulse = <?php echo $pulse; ?>

var pulseLabels = <?php echo $pulseLabels; ?>

</script>