<div class="bright-background ow-fluid-section container-fluid" >
<div class="section-header">
				<div class="section-header-left col-md-5"></div>
					<h2 class="section-title col-md-2"> <span class="seperate-title">Find</span> <b>customer</b> </h2>
				<div class="section-header-right col-md-5"></div>
			</div>
			
<div class="container">	
<table id="example" class="table table-striped" >
  	<thead>
  		<tr>
  			<th><?php echo GeneralMessage::FirstName; ?></th>
  			<th><?php echo GeneralMessage::LastName; ?></th>
  			<th><?php echo GeneralMessage::Email; ?></th>
  			<th><?php echo GeneralMessage::Login; ?></th>
  			<th><?php echo GeneralMessage::City; ?></th>
  			<th><?php echo GeneralMessage::LockingState; ?></th>
  			<th><?php echo GeneralMessage::Change; ?></th>
  		</tr>
  	</thead>
  	<tbody>	
  		<?php 
  		foreach ($customers as $customer) { ?>
  		<tr>
  			<td><?php echo $customer->getFirstName(); ?></td>
  			<td><?php echo $customer->getLastName(); ?></td>
  			<td><?php echo $customer->getEmail(); ?></td>
  			<td><?php echo $customer->getLogin(); ?></td>
  			<td><?php echo $customer->getCity(); ?></td>
  			<td><?php echo $customer->getStatusReadable(); ?></td>
  			<td><a href="?site=AdministrateUserEdit&edit=<?php echo $customer->getId();?>">Edit</a></td>
  		</tr>
  		<?php } ?>
  	</tbody>
	</table>
</div>