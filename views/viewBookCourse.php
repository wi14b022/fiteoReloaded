
<div class="bright-background ow-fluid-section container-fluid">


<div class="section-header">
				<div class="section-header-left col-md-5"></div>
					<h2 class="section-title col-md-2"> <span class="seperate-title">get a</span> <b>Meeting</b> </h2>
				<div class="section-header-right col-md-5"></div>
			</div>

		
				
<div class="container">			
<div class="row">
	<div class="col-sm-7">
		<form class="form-inline" id="paypal" action="" method="post">
			<div class="form-group">
				<div class="col-sm-9">
					<select class="form-control" id="location" name="location">
						<option <?php echo selected('Bicycling',''); ?> value="">All Locations</option>
						<?php foreach ($shops as $shop) {?>
						<option value="<?php echo $shop->getId();?>"><?php echo $shop->getCity().', '.$shop->getStreet(); ?></option>
						<?php } ?>
					</select>
				</div>
			</div>			
		</form>	
	</div>
</div>



</div>


			<div class="ow-fixed-section container">
				<div id="events" class="events-list-block col-md-8 col-lg-9">			
					
					
				</div>
				
				<div class="event-calendar-block col-md-4 col-lg-3">
					<!-- Responsive calendar - START -->
					<div class="responsive-calendar">
						<div class="today-events"  style="height: 0px;">
							<div class="col-xs-4 col-md-5 col-lg-4">
							</div>
							<div class="col-xs-8 col-md-7 col-lg-8">

							</div>
						</div>
						<div class="controls">
							<a class="pull-left" data-go="prev"><div class="btn"><i class="fa fa-angle-left"></i></div></a>
							<p><span data-head-month></span> <span data-head-year></span></p>
							<a class="pull-right" data-go="next"><div class="btn"><i class="fa fa-angle-right"></i></div></a>
						</div>
						<hr/>
						<div class="day-headers">	
							<div class="day header">Sun</div>
							<div class="day header">Mon</div>
							<div class="day header">Tue</div>
							<div class="day header">Wed</div>
							<div class="day header">Thu</div>
							<div class="day header">Fri</div>
							<div class="day header">Sat</div>						
							<hr/>
						</div>
						<div class="days" data-group="days">
						<!-- the place where days will be generated -->
						</div>

					</div>
					<!-- Responsive calendar - END -->
				</div>
			</div><!-- /.container -->
		</div><!-- /.container-fluid -->
