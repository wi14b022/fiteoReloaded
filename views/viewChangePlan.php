

<div class="bright-background ow-fluid-section container-fluid">
	
	<div class="section-header">
					<div class="section-header-left col-md-5"></div>
						<h2 class="section-title col-md-2"> <span class="seperate-title">change a</span> <b>diet</b> & <b>training plan</b> </h2>
					<div class="section-header-right col-md-5"></div>
				</div>
	
	<div class="container">	
		<div class="row">
			<div class="col-sm-6">
			<table id="example" class="table table-striped">
			  	<thead>
			  		<tr>
			  			<th>#</th>
			  			<th><?php echo GeneralMessage::Workout; ?></th>
			  			<th>Plantyp</th>
			  			<th><?php echo GeneralMessage::Change; ?></th>
			  		</tr>
			  	</thead>
			  	<tbody>	
			  		<?php 
			  		$i=1;
			  		$x=0;
			  		foreach ($currentUser->getUnlockedProducts() as $plan) { 
			  		 if ($plan instanceof Plan) {
			  			?>
			  		<tr>
			  			<td><?php echo $i++; ?></td>
			  			<td><?php echo $plan->getName(); ?></td>
			  			<td><?php echo $plan->getPlanType(); ?></td>
			  			<td><a href="?site=changePlan&plan=<?php echo $x; ?>">Request a change</a></td>
			  		</tr>
			  		<?php 

			  		 }
						$x++;
					} ?>
			  	</tbody>
				</table>
			</div>
			
			<div class="col-sm-6">
				<form class="form-horizontal" action="" method="post">
					<div class="form-group">
						<label for="example1" class="col-sm-3 control-label">Plan</label>
						<div class="col-sm-9 ">
						<div class="form-control" disabled>
							<?php echo $editplan->getName(); ?>
						</div>
							
						</div>
					</div>
				
				
					<div class="form-group">
						<label for="example1" class="col-sm-3 control-label">Trainer</label>
						<div class="col-sm-9">
							<select class="form-control" id="trainer" name="trainer" <?php echo $disabled; ?>>
								<?php foreach ($trainersWithoutDummy as $trainer) {?>
								<option <?php echo selected($currentUser->getTrainer(), $trainer->getId()); ?> value="<?php echo $trainer->getId();?>"><?php echo $trainer->getFullName(); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="example1" class="col-sm-3 control-label">Comment</label>
						<div class="col-sm-9">
							 <textarea class="form-control" rows="10" id="notes" name="notes" <?php echo $disabled; ?>></textarea>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 15%;">
						<div class=" text-right">
							<input type="hidden" id="plan" name="plan" value="<?php echo $plan->getId(); ?>" /> <input
								type="hidden" id="save" name="save" value="create" />
							<button type="submit" class="btn btn-default" <?php echo $disabled; ?>>Send request</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
