
<div class="bright-background ow-fluid-section container-fluid">


<div class="section-header">
				<div class="section-header-left col-md-5"></div>
					<h2 class="section-title col-md-2"> <span class="seperate-title">check my</span> <b>training records</b> </h2>
				<div class="section-header-right col-md-5"></div>
			</div>

		
				
<div class="container">			

<div class="row">
  <div class="col-md-7">
  	<table id="example" class="table table-striped">
  	<thead>
  		<tr>
  			<th>#</th>
  			<th><?php echo GeneralMessage::Workout; ?></th>
  			<th><?php echo GeneralMessage::Hours; ?></th>
            <th>Recorder</th>
  			<th><?php echo GeneralMessage::Change; ?></th>
            <th style="display: none;"></th>
  		</tr>
  	</thead>
  	<tbody>	
  		<?php 
  		$i=1;
  		$etrCounter=0;
  		foreach ($currentUser->getUserETRs() as $etr) { ?>
  		<tr <?php if($etr->getRecorder()=='C'){ ?> class="clickable-row" <?php } ?> >
  			<td><?php echo $i; ?></td>
  			<td><?php echo $etr->getRecordTypeText(); ?></td>
  			<td><?php echo $etr->getDuration(); ?></td>
            <td><?php echo $etr->getRecorderValue() ?> </td>
  			<td><?php if($etr->getRecorder()=='C') { ?>
                <a href="?site=ETR&changeLock=<?php echo $i; ?>"><?php echo $etr->getInverseLockingStateText(); ?></a>
  			/ <a href="?site=ETR&edit=<?php echo $i; ?>">Edit</a>
            <?php } ?>
            </td>
            <td style="display: none;"><?php echo $etr->getTrainer() ?></td>
  		</tr>
  		<?php 
  		$i++;
  		} ?>
  	</tbody>
	</table>
	
	
	
  </div>
  <div class="col-md-5">
  	<form class="form-horizontal" action="?site=ETR" method="post">
	  
	  <div class="form-group">
	    <label for="sport" class="col-sm-2 control-label">Workout</label>
	    <div class="col-sm-10">
	      <select class="form-control" id="workout" name="workout">
			  <option value="A" <?php echo selected('A', $editEtr->getRecordType()); ?>>Endurance training</option>
			  <option value="K" <?php echo selected('K', $editEtr->getRecordType()); ?>>Strength training</option>
			</select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="duration" class="col-sm-2 control-label">Duration</label>
	    <div class="col-sm-10">
	      <select class="form-control" id="duration" name="duration">
	      	  <option value="10" <?php echo selected(10, $editEtr->getDuration()); ?>>10 Minutes</option>
			  <option value="15" <?php echo selected(15, $editEtr->getDuration()); ?>>15 Minutes</option>
			  <option value="30" <?php echo selected(30, $editEtr->getDuration()); ?>>30 Minutes</option>
			  <option value="45" <?php echo selected(45, $editEtr->getDuration()); ?>>45 Minutes</option>
			  <option value="60" <?php echo selected(60, $editEtr->getDuration()); ?>>1 Hour</option>
			  <option value="90" <?php echo selected(90, $editEtr->getDuration()); ?>>1,5 Hour</option>
			  <option value="120" <?php echo selected(120, $editEtr->getDuration()); ?>>2 Hours</option>
			  <option value="150" <?php echo selected(150, $editEtr->getDuration()); ?>>2,5 Hours</option>
			</select>
	    </div>
	  </div>
        <div class="form-group">
            <label for="duration" class="col-sm-2 control-label">Unlock to</label>
            <div class="col-sm-10">
                <select class="form-control" id="trainer" name="trainer">
                    <option <?php echo selected($editEtr->getTrainer(), ""); ?>
                        value="NULL">none</option>
                    <?php foreach ($trainersWithoutDummy as $trainer) {?>
                        <option
                            <?php echo selected($editEtr->getTrainer(), $trainer->getId()); ?>
                            value="<?php echo $trainer->getId();?>"><?php echo $trainer->getFullName(); ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10 text-right">
	      <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
	      <input type="hidden" id="save" name="save" value="create" />
	      <input type="hidden" id="site" value="ETR" />
	      <button id="submit" type="submit" class="btn btn-default"><?php echo $saveText; ?></button>
	    </div>
	  </div>
	</form>
  </div>
</div>

  </div>
</div>