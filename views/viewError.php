<div class="bright-background ow-fluid-section container-fluid">


    <div class="section-header">
        <div class="section-header-left col-md-5"></div>
        <h2 class="section-title col-md-2">  <b>Error</b> <span class="seperate-title">occured </span> </h2>
        <div class="section-header-right col-md-5"></div>
    </div>
    <div class="container align-center">
        <h1>Error </h1>
        The application has encountered an known error. <br />
        It doesn't appear to have affected your data, but our technical <br />
        staff have been automatically notified and will be looking <br />
        into this with the utmost urgency. <br />
    </div>
</div>