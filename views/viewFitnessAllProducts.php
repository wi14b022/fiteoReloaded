<?php 
require_once 'logic/logicFitness.php';
?>


<div id="classes-programs" class="classes-programs ow-fluid-section container-fluid">
			<h2 class="section-title"> <span class="seperate-title">these are your</span> <b>diet</b> & <b>training programs</b> </h2>
			<div class="ow-fixed-section container">
				<ul id="owl-training-programs" class="training-programs">
					<?php 
					$i=0;
					foreach ($currentUser->getUnlockedProducts() as $product)
					{
					?>
				
					<li class="program-item">
						<div class="program-item-inner">
							<a href="#">
								<img src="<?php echo $product->getThumbnail();?>" alt="classes programs thumbnail">
							</a>
							<h4 class="program-title"><a href="#"><?php echo $product->getName(); ?></a></h4>
							<hr/>
							<div class="programs-meta col-md-12">
								<div class="programs-info col-md-12 col-lg-6">
									<span class="program-trainer"><i class="trainer-icon"></i><?php echo $product->getPlanType(); ?></span>
									<?php if ($product instanceof Plan) { ?>
									<div class="program-date"><i class="calendar-icon"></i><?php echo $product->getSportType(); ?></div>
									<?php }
									else echo '<div class="program-date"  style="visibility: hidden;"><i class="calendar-icon"></i>-</div>';
									?>
								</div>
								<p></p>
								<a title="join it" class="btn btn-hermes" href="?site=Fitness&id=<?php echo $i; ?>">View <?php echo $product->getType(); ?></a>
								
							</div>						
						</div>
					</li>
					<?php 
					$i++;
					}
					?>
				</ul>
			</div><!-- /.container -->
		</div><!-- /.container-fluid -->

</div>

