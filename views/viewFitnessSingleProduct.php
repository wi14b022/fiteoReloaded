<?php 
require_once 'logic/logicFitness.php';
?>


<div class="bright-background ow-fluid-section container-fluid">
<div class="section-header">
				<div class="section-header-left col-md-5"></div>
					<h2 class="section-title col-md-2"> <span class="seperate-title">enjoy</span> <b><?php echo $product->getName();?></b> </h2>
				<div class="section-header-right col-md-5"></div>
			</div>
	<div class="ow-fixed-section container">
	
		<div class="container">	
			<div class="row">
				<div class="col-sm-6">
				<table id="example" class="table table-striped">
				  	<thead>
				  		<tr>
				  			<th>#</th>
				  			<th><?php echo GeneralMessage::Workout; ?></th>
				  			<th>Revision</th>
				  			<th>Quantity</th>
				  			<th>Duration</th>
				  			<th>Start weight</th>
				  			<th><?php echo GeneralMessage::Change; ?></th>
				  		</tr>
				  	</thead>
				  	<tbody>	
				  		<?php 
				  		$i=1;
				  		foreach ($product->getExercises() as $exercise) { 
				  			?>
				  		<tr>
				  			<td><?php echo $i++; ?></td>
				  			<td><?php echo $exercise->getName(); ?></td>
				  			<td><?php echo $exercise->getRevision(); ?></td>
				  			<td><?php echo $exercise->getQuantity(); ?></td>
				  			<td><?php echo $exercise->getDuration(); ?></td>
				  			<td><?php echo $exercise->getStartWeight(); ?></td>
				  			<td><a href="?site=Fitness&id=<?php echo $id; ?>&order=<?php echo $exercise->getOrder(); ?>">See details</a></td>
				  		</tr>
				  		<?php 
				  		
				  		} ?>
				  	</tbody>
				</table>
			</div>
			
			<div class="col-sm-6">
				<?php if (isset($singleExercise)) { ?>
					<div class="col-sm-12"><h2><?php echo $singleExercise->getName(); ?></h2>
					<p><?php echo $singleExercise->getDescription(); ?></p>
					
					
					<?php if (!empty($singleExercise->getRevision() )){ ?>
						<p><strong>Revisions: </strong> <?php echo $singleExercise->getRevision(); ?></p>
					<?php } ?>
					<?php if (!empty($singleExercise->getQuantity() )){ ?>
						<p><strong>Quantity: </strong> <?php echo $singleExercise->getQuantity(); ?></p>
					<?php } ?>
					<?php if (!empty($singleExercise->getDuration() )){ ?>
						<p><strong>Duration: </strong> <?php echo $singleExercise->getDuration(); ?></p>
					<?php } ?>
					<?php if (!empty($singleExercise->getStartWeight() )){ ?>
						<p><strong>Start weight: </strong> <?php echo $singleExercise->getStartWeight(); ?></p>
					<?php } ?>
					
					<?php if (!empty($singleExercise->getMedienContent() )){ ?>
						<p><img src="<?php echo $singleExercise->getMedienContent()->getUrl(); ?>" /></p>
					<?php } ?>
					
					</div>
				<?php } ?>
			</div>
			
	</div><!-- /.container -->
</div><!-- /.container-fluid -->

</div>

