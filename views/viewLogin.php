<div class="bright-background ow-fluid-section container-fluid">

<div class="section-header">
				<div class="section-header-left col-md-5"></div>
					<h2 class="section-title col-md-2"> <span class="seperate-title">User</span> <b>Login</b> </h2>
				<div class="section-header-right col-md-5"></div>
			</div>
<br>
<div class="container">
<form class="form-horizontal" method="post" action="?site=login">
    
    <div class="form-group">
		<label for="login" class="col-md-2 control-label">Login name</label>
		<div class="col-sm-9">
			<input id="login" name="login" class="form-control" placeholder="Login name"
				required="required" />
		</div>
	</div>
  
    <div class="form-group">
		<label for="password" class="col-md-2 control-label">Password</label>
		<div class="col-sm-9">
			<input id="password" name="password" type="password" class="form-control" placeholder="Password"
				required="required"  />
		</div>
	</div>
	<p class="text-right col-sm-11">
  		<button type="submit" class="btn btn-default">Sign in</button>
  	</p>
</form>
</div></div>
