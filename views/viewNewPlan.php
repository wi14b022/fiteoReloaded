
<div class="bright-background ow-fluid-section container-fluid">


<div class="section-header">
				<div class="section-header-left col-md-5"></div>
					<h2 class="section-title col-md-2"> <span class="seperate-title">buy a</span> <b>Fitpoint Package</b> </h2>
				<div class="section-header-right col-md-5"></div>
			</div>

			
<div class="row">
	<div class="col-sm-7">
		<form class="form-horizontal" id="paypal" action="" method="post">
			<div class="form-group">
				<label for="example1" class="col-sm-3 control-label">Package</label>
				<div class="col-sm-9">
					<div id="sandbox-container">
						<select class="form-control" id="package" name="package">
								<?php
								foreach ($fitpointPackages as $fitpointPackage){
									?>
										<option value="<?php echo $fitpointPackage->getId(); ?>" >
											<?php echo $fitpointPackage->getName(); ?>
										</option>
									<?php
								}
								?>
							</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="login" class="col-sm-3 control-label">Holder</label>
				<div class="col-sm-9">
					<input id="firstname" name="firstname" class="form-control"
						placeholder="Firstname" required="required" value="<?php echo $editUser->getFirstName(); ?>"
						pattern=".{5,20}" required title="5 to 20 characters" />
					<input id="lastname" name="lastname" class="form-control"
						placeholder="Lastname" required="required"
						pattern=".{5,20}" required title="5 to 20 characters" value="<?php echo $currentUser->getLastName(); ?>" />
				</div> 
			</div>
			<div class="form-group">
				<label for="example1" class="col-sm-3 control-label">Creditcard-Type</label>
				<div class="col-sm-9">
					<select class="form-control" id="creditcardtype" name="creditcardtype">
						<option <?php echo selected('Bicycling',''); ?> value="visa">VISA</option>
						<option <?php echo selected('Bicycling',''); ?> value="mastercard">Master Card</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="login" class="col-sm-3 control-label">CreditCard Number</label>
				<div class="col-sm-9">
					<input id="ccNumber" name="ccNumber" class="form-control"
						placeholder="Creditcard number" value="<?php  ?>"
						required="required" pattern=".{5,20}" required
						title="5 to 20 characters" <?php  ?> />
				</div>
			</div>
			<div class="form-group">
				<label for="login" class="col-sm-3 control-label">CreditCard Expire
					Date</label>
				<div class="col-sm-9">
					<input id="month" name="month" class="form-control"
						placeholder="Month" value="05" required="required"
						pattern=".{2,2}" type="number" min="01" max="12" required title="2 to 2 characters" <?php  ?> />
					<input id="year" name="year" class="form-control"
						placeholder="Year" value="2016" required="required"
						pattern=".{4,4}" type="number" min="2016" max="2025" required title="4 to 4 characters" <?php  ?> />
				</div>
			</div>
			<div class="form-group">
				<label for="cvv" class="col-sm-3 control-label">CVV</label>
				<div class="col-sm-9">
					<input id="cvv" name="cvv" class="form-control"
						placeholder="CVV" value="<?php  ?>" required="required"
						pattern=".{3,3}" required title="5 to 20 characters" <?php  ?> />
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10  text-right">
					<button type="submit" id="sendButton" class="btn btn-default" ><?php echo GeneralMessage::Save; ?></button>
				</div>
			</div>
			
		</form>
		
		
	</div>
	<div id="status"></div>



</div>
</div>