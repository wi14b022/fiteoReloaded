<div class="slider-section slider-style-1">
			<div class="slider-block-container">
				<div id="header-1-flex" class="flexslider header-1-flex">
					<ul class="slides">
						<!-- Slide Item -->
						<li>
							<div class="slider-image">
								<img src="images/slider/header1-1920x800/5.jpg" alt="Maya Toitovna"/>
							</div>
							<div class="slider-content">
								<h1 class="slide-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.7s"> <a href="#">My body is strong.</a></h1>
								<span class="slide-sub-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.9s"> SO IS My will. </span>
								<p class="slide-description wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.1s"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
								<hr/>
								<span class="person wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.3s"> Maya Toitovna </span>
								<em class="person-detail wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.4s"> fitness champion 2014 </em>
								<img class="pull-left wow fadeIn signature-image" src="images/header/header-1/signature.png" alt="signature Maya Toitovna" title="signature Maya Toitovna" data-wow-duration="0.8s" data-wow-delay="1.5s"/>
								<a href="#" class="btn btn-hermes pull-right scroll  wow fadeInUp" title="See Classes" data-wow-duration="0.8s" data-wow-delay="1.6s"> see classes </a>
							</div>
						</li>
						<li>
							<div class="slider-image">
								<img src="images/slider/header1-1920x800/4.jpg" alt="Maya Toitovna"/>
							</div>
							<div class="slider-content">
								<h1 class="slide-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.7s"> <a href="#">My body is strong.</a> </h1>
								<span class="slide-sub-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.9s"> SO IS My will. </span>
								<p class="slide-description wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.1s"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
								<hr/>
								<span class="person wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.3s"> Maya Toitovna </span>
								<em class="person-detail wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.4s"> fitness champion 2014 </em>
								<img class="pull-left wow fadeIn signature-image" src="images/header/header-1/signature.png" alt="signature Maya Toitovna" title="signature Maya Toitovna" data-wow-duration="0.8s" data-wow-delay="1.5s"/>
								<a href="#" class="btn btn-hermes pull-right scroll  wow fadeInUp" title="See Classes" data-wow-duration="0.8s" data-wow-delay="1.6s"> see classes </a>
							</div>
						</li>
						<li class="slide-item">
							<div class="slider-image">
								<img src="images/slider/header1-1920x800/3.jpg" alt="Maya Toitovna"/>
							</div>
							<div class="slider-content">
								<h1 class="slide-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.7s"> <a href="#">My body is strong.</a> </h1>
								<span class="slide-sub-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.9s"> SO IS My will. </span>
								<p class="slide-description wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.1s"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
								<hr/>
								<span class="person wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.3s"> Maya Toitovna </span>
								<em class="person-detail wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.4s"> fitness champion 2014 </em>
								<img class="pull-left wow fadeIn signature-image" src="images/header/header-1/signature.png" alt="signature Maya Toitovna" title="signature Maya Toitovna" data-wow-duration="0.8s" data-wow-delay="1.5s"/>
								<a href="#" class="btn btn-hermes pull-right scroll  wow fadeInUp" title="See Classes" data-wow-duration="0.8s" data-wow-delay="1.6s"> see classes </a>
							</div>
						</li>
						<li class="slide-item">
							<div class="slider-image">
								<img src="images/slider/header1-1920x800/2.jpg" alt="Maya Toitovna"/>
							</div>
							<div class="slider-content">
								<h1 class="slide-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.7s"> <a href="#">My body is strong.</a> </h1>
								<span class="slide-sub-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.9s"> SO IS My will. </span>
								<p class="slide-description wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.1s"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
								<hr/>
								<span class="person wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.3s"> Maya Toitovna </span>
								<em class="person-detail wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.4s"> fitness champion 2014 </em>
								<img class="pull-left wow fadeIn signature-image" src="images/header/header-1/signature.png" alt="signature Maya Toitovna" title="signature Maya Toitovna" data-wow-duration="0.8s" data-wow-delay="1.5s"/>
								<a href="#" class="btn btn-hermes pull-right scroll  wow fadeInUp" title="See Classes" data-wow-duration="0.8s" data-wow-delay="1.6s"> see classes </a>
							</div>
						</li>
						<li class="slide-item">
							<div class="slider-image">
								<img src="images/slider/header1-1920x800/1.jpg" alt="Maya Toitovna"/>
							</div>
							<div class="slider-content">
								<h1 class="slide-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.7s"> <a href="#">My body is strong.</a> </h1>
								<span class="slide-sub-heading wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.9s"> SO IS My will. </span>
								<p class="slide-description wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.1s"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. </p>
								<hr/>
								<span class="person wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.3s"> Maya Toitovna </span>
								<em class="person-detail wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1.4s"> fitness champion 2014 </em>
								<img class="pull-left wow fadeIn signature-image" src="images/header/header-1/signature.png" alt="signature Maya Toitovna" title="signature Maya Toitovna" data-wow-duration="0.8s" data-wow-delay="1.5s"/>
								<a href="#" class="btn btn-hermes pull-right scroll  wow fadeInUp" title="See Classes" data-wow-duration="0.8s" data-wow-delay="1.6s"> see classes </a>
							</div>
						</li>
					</ul>
					<!--nav class="slides-navigation slide-pagination">					
						<a href="#" class="prev slide-prev">Previous</a>
						<a href="#" class="next slide-next">Next</a>
					</nav-->
					<!-- Slider /- -->
				</div>
			</div>
		</div>
		
		<div id="content-box" class="content-box ow-fluid-section container-fluid">
			<div class="ow-fixed-section container owl-wrapper-outer">
				<div id="owl-cntbox" class="content-box-inner">
					<div class="content-box-item cntbox">
						<a href="#"><h2 class="content-box-title"> Trainings for both </h2></a>
						<div class="content-box-detail">
							<a href="#"><img src="images/content-box/image_1.jpg" title="Trainings for both Image" alt="Trainings for both Image"/></a>
							<p class="content-box-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<hr/>
							<ul>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/circle.png" alt="content-box service icon"/></i>
									<span> Holistic approach </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/watch.png" alt="content-box service icon"/></i>
									<span> Fast results </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/leaf.png" alt="content-box service icon"/></i>
									<span> Nutrition advices </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/scale.png" alt="content-box service icon"/></i>
									<span> Weightwatch </span>
								</li>
							</ul>
						</div>
					</div>
					<div class="content-box-item cntbox">					
						<a href="#"><h2 class="content-box-title"> Skilled trainers </h2></a>
						<div class="content-box-detail">
							<a href="#"><img src="images/content-box/image_2.jpg" title="Trainings for both Image" alt="Trainings for both Image"/></a>
							<p class="content-box-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<hr/>
							<ul>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/fire.png" alt="content-box service icon"/></i>
									<span> Motivational </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/rocket.png" alt="content-box service icon"/></i>
									<span> Personalized </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/dialog.png" alt="content-box service icon"/></i>
									<span> Counseling </span>
								</li>
							</ul>
						</div>
					</div>
					<div class="content-box-item cntbox">
						<a href="#"><h2 class="content-box-title"> Yoga Classes </h2></a>
						<div class="content-box-detail">
							<a href="#"><img src="images/content-box/image_3.jpg" title="Trainings for both Image" alt="Trainings for both Image"/></a>
							<p class="content-box-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<hr/>
							<ul>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/circle.png" alt="content-box service icon"/></i>
									<span> Holistic approach </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/watch.png" alt="content-box service icon"/></i>
									<span> Fast results </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/leaf.png" alt="content-box service icon"/></i>
									<span> Nutrition advices </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/scale.png" alt="content-box service icon"/></i>
									<span> Weightwatch </span>
								</li>
							</ul>
						</div>
					</div>
					<div class="content-box-item cntbox">
						<a href="#"><h2 class="content-box-title"> Pilates </h2></a>
						<div class="content-box-detail">
							<a href="#"><img src="images/content-box/image_4.jpg" title="Trainings for both Image" alt="Trainings for both Image"/></a>
							<p class="content-box-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<hr/>
							<ul>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/circle.png" alt="content-box service icon"/></i>
									<span> Holistic approach </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/watch.png" alt="content-box service icon"/></i>
									<span> Fast results </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/leaf.png" alt="content-box service icon"/></i>
									<span> Ipsum counseling </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/star.png" alt="content-box service icon"/></i>
									<span> Lorem ipsum </span>
								</li>
							</ul>
						</div>
					</div>
					<div class="content-box-item cntbox">
						<a href="#"><h2 class="content-box-title"> Yoga Classes </h2></a>
						<div class="content-box-detail">
							<a href="#"><img src="images/content-box/image_1.jpg" title="Trainings for both Image" alt="Trainings for both Image"/></a>
							<p class="content-box-description"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
							<hr/>
							<ul>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/fire.png" alt="content-box service icon"/></i>
									<span> Motivational </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/rocket.png" alt="content-box service icon"/></i>
									<span> Personalized </span>
								</li>
								<li class="col-md-12 col-lg-6">
									<i><img src="images/content-box/icons/dialog.png" alt="content-box service icon"/></i>
									<span> Counseling </span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.container-fluid -->