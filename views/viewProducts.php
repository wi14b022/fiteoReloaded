<?php 
require_once 'logic/logicProducts.php';
?>

<div id="classes-programs" class="classes-programs ow-fluid-section container-fluid">
			<h2 class="section-title"> <span class="seperate-title">these are our</span> <b>diet</b> & <b>training programs</b> </h2>
			<div class="ow-fixed-section container">
				<ul id="owl-training-programs" class="training-programs">
					<?php 
					foreach ($buyableProducts as $product)
					{
					?>
				
					<li class="program-item" ">
						<div class="program-item-inner" >
							<a href="#">
								<img src="<?php echo $product->getThumbnail();?>" alt="classes programs thumbnail">
							</a>
							<h4 class="program-title"><a href="#"><?php echo $product->getName(); ?></a></h4>
							<hr/>
							<div class="programs-meta col-md-12">
								<div class="programs-info col-md-12 col-lg-6">
									<?php if ($product instanceof Plan) { ?>
									<div class="program-date"><i class="calendar-icon"></i><?php echo $product->getSportType(); ?></div>
									<?php } ?>

									<span class="program-trainer"><i class="trainer-icon"></i><?php echo $product->getPlanType(); ?></span>
                                    <?php if ($product instanceof Video) { ?>
                                    <div class="program-date" style="visibility: hidden;"><i class="calendar-icon">-</i></div>
                                    <?php } ?>
								</div>
								<div class="programs-rate col-md-12 col-lg-6" style="text-align: right;">
									<span class="style-super currency"></span><span class="style-bold"><?php echo $product->getFitpoints(); ?></span><span class="style-super">.00</span><span class="pie-sign">/</span><span class="month">fitpoints</span>
								</div>
								<p style="min-height: 180px;"> <?php if ($product instanceof Plan) echo $product->getDescription(); ?> </p>
								<?php if ($currentUser->getFitpoints()>=$product->getFitpoints()) { ?>
								<a title="join class" class="btn btn-hermes"  href="?site=Products&<?php echo $product->getBuyLink();?>"> buy Now </a>
								<?php } 
								else { ?>
								<a title="join class" disabled="true" class="btn btn-hermes">Not enough FP</a>
								<?php } ?>
								
							</div>						
						</div>
					</li>
					<?php 
					}
					?>
					
				</ul>
			</div><!-- /.container -->
		</div><!-- /.container-fluid -->

</div>


