
<div
	class="bright-background ow-fluid-section container-fluid">


	<div class="section-header">
		<div class="section-header-left col-md-5"></div>
		<h2 class="section-title col-md-2">
			<span class="seperate-title">check my</span> <b>training records</b>
		</h2>
		<div class="section-header-right col-md-5"></div>
	</div>



	<div class="container align-center" style="min-height: 200px;">
        <?php if ($showGraphics) { ?>
		<h1>Muscle mass</h1>
		<canvas id="muscleMass" width="1200" height="400"></canvas>
		
		<h1>Adipose</h1>
		<canvas id="apodis" width="1200" height="400"></canvas>
		
		<h1>Pulse</h1>
		<canvas id="pulse" width="1200" height="400"></canvas>
        <?php }
        else echo "<br>No Trainingsrecords found";
        ?>
	</div>

</div>